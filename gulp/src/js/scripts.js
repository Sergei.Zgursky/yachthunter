$(document).on('click', '.js-anhor', function(){
    var target = $(this).attr('href');
    $('html, body').animate({scrollTop: $(target).offset().top}, 1000);
    return false;
})
$(document).on('click', '.js-favourites', function() {
    $(this).toggleClass('is-active');
    if($(this).hasClass('is-active')){
        $(this).text('В избранном')
        $(this).find('.card__info-link-hidd').show(100);
        setTimeout(function() {
          $('.card__info-link-hidd').hide(100);
        }, 2500);
    }else {
        $(this).text('В избранное')
    }
})
$('.card__detailed').append('<div class="card__detailed_hidd"/>');
$('.card__price-ok').clone().addClass('card__price-ok_mob').appendTo($('.card__detailed_hidd'));
$('.card__price').clone().addClass('card__price_mob').appendTo($('.card__detailed_hidd'));

$('.js-link-open-spec').closest('ul').find('li').eq(1).nextAll('li').addClass('hidd_mob');
$(document).on('click', '.js-link-open-spec', function(){
    $(this).closest('ul').toggleClass('open_mob');
    return false
});

$(document).on('click', '.js-card__info-dd-link', function(){
    $(this).closest('li').toggleClass('open_spec');
    return false
});

$('.catalog__li').each(function(){
    
    
    var catalogBox = $(this).find('.catalog__img-box');
    $(this).find('.catalog__dl_price-ok').clone().appendTo(catalogBox);
    $('<div class="catalog__img-box-text"/>').appendTo(catalogBox);
    var catalogBoxImg = $(this).find('.catalog__img-box-text');
    $(this).find('.catalog__dl_cabins').clone().appendTo(catalogBoxImg);
    $(this).find('.catalog__dl_passengers').clone().appendTo(catalogBoxImg);
    $(this).find('.catalog__img-box-text .catalog__dl_cabins .catalog__dd').prepend('<svg class="icon-cabins">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-cabins"></use>' +
                                                                '</svg>')
    $(this).find('.catalog__img-box-text .catalog__dl_passengers .catalog__dd').prepend('<svg class="icon-cabins">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-passengers"></use>' +
                                                                '</svg>')
    var yearText = $(this).find('.catalog__dl_year .catalog__dd').text();
    $(this).find('.catalog__dl_size .catalog__dd').append('<span class="catalog__dl_year-title"> ||  '+ yearText +'</span>');
    
    var catalogName = $(this).find('.catalog__name').text();
    $(this).find('.catalog__name').attr('data-name', catalogName);
}); 
if($(window).width() > 630 ) {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
    $('.catalog__li:not(.swiper-slide)').eq(1).addClass('catalog__li_active');
}else {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
}
$.fn.catalogActive = function () {
	var windowHeight = $(window).height();
    $('.catalog__li:not(.swiper-slide)').each(function() {
        var $this = $(this),
            height = $this.offset().top + $this.height(),
            heightRem = $this.offset().top - $this.height();
        if($(window).width() > 630 ) {
            if ($(document).scrollTop() + windowHeight >= height) {
                $this.addClass('catalog__li_active');
            }else {
                $this.removeClass('catalog__li_active');
            }            
        } else {
            if ($(document).scrollTop() + windowHeight >= height) {
                $this.addClass('catalog__li_active').siblings('.catalog__li_active').removeClass('catalog__li_active');
            }
        }
    });
    
}
$(document).on('scroll', function(){
    $.fn.catalogActive()
})
$(document).on('load', function(){
    $.fn.catalogActive()
})

$(document).on('click', '.catalog__flag', function() {
    $(this).toggleClass('is-active');
    if($(this).hasClass('is-active')){
        $(this).find('.catalog__flag-hide').show(100);
        setTimeout(function() {
          $('.catalog__flag-hide').hide(100);
        }, 2500);
    }
})
    
$(document).on('click', '.js-control', function() {
    $(this).addClass('is-active').siblings('.js-control').removeClass('is-active');
    if($(this).hasClass('tile')){
        $('.catalog').addClass('catalog_tile');
    } else {
        $('.catalog').removeClass('catalog_tile');
    }
    $.catalogLoad();
    return false
});

if ($('.page__search .catalog').length) {
    $(document).on('load resize', function(){
        
    })
}

$.catalogLoad = function() {
    var load_more = false;

    var currentPage = 1;
    var baseUrl = window.location.href;
    if(baseUrl.indexOf('page')>= 0){
        var namberPage = parseInt(baseUrl.match(/page=(\d+)/)[1]);
        currentPage = namberPage;
        baseUrl = baseUrl.replace('&page=' + namberPage, '');
        history.pushState(null, null, baseUrl); 
    }
    $(window).scroll(function() {
        if($(".prelod-page:not(.ok)").length && !load_more) {
            var offset_button = $(".catalog li").last().offset();
            if($(this).scrollTop() >= offset_button.top - $(window).height()) {
                currentPage++;
                
                var r = Math.random();
                if(baseUrl.indexOf('?')== -1){
                    var numberPage = '?&page='
                }else {
                    var numberPage = '&page=';
                }
                var  newUrl = baseUrl + numberPage + currentPage;
                history.pushState(null, null, newUrl);                   
                var sendUrl = baseUrl + numberPage + currentPage + '&' + r;
                load_more = true;
                $('.prelod-page').show();
                $.ajax({
                    url: sendUrl,
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if(!(response == "")) {
                            $('.catalog__list').append(response);
                            $('.prelod-page').hide();
                            load_more = false; 
                        }
                        if(response == "") {
                            $('.prelod-page').addClass('ok').hide();
                            load_more = false;
                            currentPage--
                            var  newUrl = baseUrl + numberPage + currentPage;
                            history.pushState(null, null, newUrl);
                            return false
                        }
                    },
                    error: function(response) {
                        $('.prelod-page').hide();
                        return false
                        load_more = false;
                    }
                });
            }
        }
    });    
};
$.catalogLoad();

$.datepickerOp = function(){
    var $el = $('.datepicker-input:not(.openDat)');
    $el.datepicker({
        range: true,
        toggleSelected: false,
        multipleDatesSeparator: '-',
        minDate: new Date(),
        autoClose: true,
        onShow: function(dp, animationCompleted){
            if (animationCompleted) {
                $el.addClass('openDat');  
            }
        },
        onHide: function(dp, animationCompleted){
            $el.removeClass('openDat');            
        }
    })    
};
$.datepickerOp();
if($('.datepicker-input_lighten').length){
    $('.datepickers-container').addClass('datepickers-container_lighten')
}

$.faqCol = function () {
    let a = 0;
    while (a < 3) { 
        $('.faq-slider__swiper-wrapper').append('<div class="swiper-slide faq-slider__swiper-slide"/>');
        a++    
    }
    var sumLinkCol = Math.ceil($('.faq__link-container-box').length / 3);
       
    let i = 0;
    while (i < sumLinkCol) { 
        var numbelLink = $('.faq__link-container-box').slice(0, sumLinkCol).appendTo($('.faq-slider__swiper-slide').eq(i))
        i++;
    }
  
}
$(function() {
    $.faqCol();
    $.faqSlider();
});
$('.faq__answer').each(function(){
    var namberEl = $(this).index();
    $(this).clone().addClass('mob').appendTo($('.faq__question').eq(namberEl))
});
$(document).on('click', '.faq__question-link', function(){
   var namberEl = $(this).closest('.faq__question').index();    $(this).closest('.faq__question').toggleClass('faq__question_active').removeClass('faq__question_siblings').siblings('.faq__question').addClass('faq__question_siblings').removeClass('faq__question_active'); 
    $('.faq__answer-box .faq__answer').eq(namberEl).toggleClass('faq__answer_active').siblings('.faq__answer').removeClass('faq__answer_active');
    
//    $('.faq__question-list .faq__answer').eq(namberEl).toggleClass('faq__answer_active').siblings('.faq__answer').removeClass('faq__answer_active');
    return false
});

$(document).on('click', '.faq__link', function(){
    $('.faq__link').removeClass('active');
    $(this).addClass('active')
    var linkId = $(this).attr('href');
    $(linkId).show(0).siblings('.faq__item').hide(0);
    return false
});
   
if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    $('html').addClass('ios')
}
var windowHeight = $(window).height();
$(window).scroll(function() {
    if($(this).scrollTop() > 10) {
        $('.header').addClass('header__sticky');
    }else {
        $('.header').removeClass('header__sticky');
    }
});
setInterval(function() {
    $('.header__control-user:not(.header__control-user_lk)').addClass('is-active');
    setTimeout(function() {
        $('.header__control-user').removeClass('is-active');
    }, 2500);
}, 12000);

if(!$('.first-screen').length) {
    $('.header').addClass('header_dark');
    $('.main').addClass('main_padd');
}
if($(window).width() <= 1024) {
    $(document).on('click', '.header__control-user_lk', function(){
        $('.header__lk').toggleClass('is-active');
        return false
    });
}
$(document).on('click', '.js-select-lang', function(){
    $(this).closest('.header__lang').toggleClass('is-open');
});
$(document).on('click', '.header__lang-hidd-option', function(){
    $(this).addClass('header__lang-hidd-option-select').siblings('.header__lang-hidd-option').removeClass('header__lang-hidd-option-select');
    var text = $(this).text();
    $('.header__lang-select').text(text);
    $(this).closest('.header__lang').removeClass('is-open');
});
$(document).on('click', function(event){
    if( $(event.target).closest('.header__lang').length) 
        return;         
    $('.header__lang').removeClass('is-open');
    event.stopPropagation();
});
$(document).on('click', '.js-lk-edit', function(){
    $(this).closest('.lk-page').addClass('lk-page-edit')
    $(this).closest('.lk-page').find('.lk-page__input__text').prop('disabled', false);
    return false
});
if ($('.search-main').length) {
    $('.search-main__button').before($('.search-main__button').clone().attr('href', '#popup__search-main').attr('data-modal', 'text').addClass('mob'));
    $('.page').append('<div class="popup popup__search-main" id="popup__search-main"/>');
    $('#popup__search-main').append($('.search-main').clone());
    var $titleMobile = $('.search-main').data('title-mobile');
    $('#popup__search-main').prepend('<p class="popup__title">'+ $titleMobile +'</p>');
    $(".select2-search__field").prop("readonly", true);
}
//Маска телефона
$.masked = function () {
    var $el = $('.phone-masked');
    var maskList = $.masksSort($.masksLoad("json/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			showMaskOnHover: false,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask"
	};
    $el.inputmasks(maskOpts); 
}
if($('.phone-masked').length) {
    $.masked();
}
$(document).on('keyup focusout focusin', '.phone-masked', function() {
    if ($(this).inputmasks('isCompleted')){
        $(this).addClass('ok')
    }else {
        $(this).removeClass('ok')            
    }
});
$(document).on('click', '.header__bt-menu', function(){
    $(this).toggleClass('is-active');
    $('.header__control-user').toggleClass('is-active');
    $('html').toggleClass('open-menu');
    if(!$('html').hasClass('open-menu')) {
        $('.main').addClass('transition');
        setTimeout(function() {
            $('.main').removeClass('transition');
        }, 600);
    }
    return false
});
$('.menu__button').before($('.menu__button').clone().attr('href', '#popup__search').attr('data-modal', 'text').addClass('mob'));

//popup
$(document).on('click', '[data-modal="text"]', function(){

    
    var src = $(this).attr('href'), 
        bt = $(this);
    $.fancybox.open({
        src: src,
        type: 'inline',
        hash: false,
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }
    });    
});


$('.enter__sn').each(function(){
    $(this).closest('.popup__flexbox').find('.submit-box').before($(this).clone().addClass('mob'))  
});
$(document).on('click', '.js-recover', function(){
    $('.enter__sn').hide();
    $('.enter__recover').show();
    $(this).hide();
    return false 
});

$(document).on('click', '.js-popup-link', function(){
    var idLink = $(this).attr('href');
    $(this).closest('.popup').find(idLink).show();
    $(this).closest('.popup__cont').hide();
    return false 
});

$.fancyOptions = {
    type: 'image',
    buttons: [
        "smallBtn"
    ],
    thumbs: {
        autoStart: true,
        axis: 'x'
    },
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    hash: false,
    btnTpl: {
       smallBtn: '<button data-fancybox-close class="fancybox-close-small-img" title="{{CLOSE}}">' +
                    '<span>Закрыть</span>' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
                 '<svg style="width: 1.19rem; height: 0.81rem; transform: rotate(180deg);">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
              '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
                 '<svg style="width: 1.19rem; height: 0.81rem;">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
            '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    },
    afterLoad: function(instance, current) {

        current.$content.closest('.fancybox-container').addClass('photo');
    }
}
$.fancy = function (el) {

    var $el = el;

    if (!$el.length) return;

    $el.attr('data-init', 'true');

    $el.fancybox($.fancyOptions);

};

//$.fancyAuto(
//    $('#popup__search-main')
//);
window.$(document).ready(function() {
    $.fancyAuto = function (el) {
        var $el = el;
        if (!$el.length) return;
    
        $el.fancybox({

            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }).trigger('click');;
    }
});



$.fn.parallax = function (resistance, mouse) {
    $el = $(this);
    $elContainer = $el.parent();
    TweenLite.to($el, 0.1, {
        x: -((mouse.clientX - $elContainer.width() * 2) / resistance)*3,
        y: -((mouse.clientY - $elContainer.height() * 2) / resistance)*3,
        ease: Power1.easeInOut
    });
};
$(document).on('mousemove', '.js-parallax-container', function(e) {
    $(this).find(".js-parallax").parallax(-50, e);
});



$(document).ready(function(){
    $('.preloader__line').on('animationend', function(){
        $('.preloader').addClass('complide');
        setTimeout(function(){
            $('.preloader__img-f').addClass('active');
        }, 800);
        setTimeout(function(){
            $('.preloader').hide(0);
        }, 1500);
    });
});
$('.search-main__box').on('focus', 'input', function() { $(this).blur(); });
var swiper = new Swiper('.scroll-container', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    mousewheel: true,
    breakpoints: {
        0: {
            direction: 'horizontal',
        },
        767: {
            direction: 'vertical',
        },
    },
});

$(document).on('propertychange change click keyup input paste', '.search__input', function(){
    var searchField = $(this).val();
    if(searchField === '')  {
        $('.search__input-result').html('').removeClass('open');
        return;
    }
    var regex = new RegExp(searchField, "i");
    
    var output = '<div>';
    var count = 1;
    $.each(data, function(key, val){
        if ((val.name.search(regex) != -1)){
            output += '<a href="#" class="search__input-link"><dl class="search__input-link-dl">';
            output += '<dt class="search__input-link-dt">' + val.name + '</dt>';
            output += '<dd class="search__input-link-dd">' + val.size + '</dd>'
            output += '</dl></a>';
            count++;
        }
    });
    output += '</div>';
    $('.search__input-result').html(output).addClass('open');
    if($('.search__input-result').find('.search__input-link').length == false) {
        $('.search__input-result').removeClass('open');
    }
    $(".search__input-link-dt").each(function(){
        $(this).html($(this).text().replace(
            new RegExp('(' + $('.search__input').val().replace(/(\[|\^|\$|\/|\\|\+|\*)/g, '\\$1') + ')', 'gi'),
            '<span>$1</span>'
        ));        
    });
});

$.select = function(el) {
    var $el = el;

    if (!$el.length) return;
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
    
    var placeholder = $el.data('placeholder'); 
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: true,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        dropdownParent: dropdownParent,
        width: '100%'
    });
}
$.selectMulte = function(el) {
    var $el = el;

    if (!$el.length) return;
    
    var placeholder = $el.attr('placeholder'); 
    
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
      
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: false,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        width: '100%',
        closeOnSelect: false,
        dropdownParent: dropdownParent,
        "language": {
           "noResults": function(){
               return "Ничего не найдено";
           }
       },
    });
    $(".select2 input").prop("readonly", true);
}
$('.multiple').each(function(){
    $.selectMulte($(this));
});
$('.select').each(function(){
    $.select($(this));
});

var swiperDirectionLit = new Swiper('.slider-direction', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    slideToClickedSlide: true,
    loopedSlides: 0,
    loop: true,
    centeredSlides: true,
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            spaceBetween: 15,
            centeredSlides: true,
        },
        768: {
            spaceBetween: 0,
            centeredSlides: true,
        },
        1024: {
            centeredSlides: true,
        },
    }
});
var swiperDirection = new Swiper('.direction__slider-big', {
    slidesPerView: 'auto',
    spaceBetween: 103,
    centeredSlides: true,
    slideToClickedSlide: true,
    loopedSlides: 0,
    loop: true,
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            spaceBetween: 15,
        },
        768: {
            spaceBetween: 103,
        },
    }
});
var swiperDirectionNav = new Swiper('.direction__slider-big-nav', {
    spaceBetween: 0,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
        },
        480: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    }
});
if($('.direction__slider-big-nav').length){
    $(document).on('click', '.direction__slider-big-nav__slide', function (e) {
        e.preventDefault();
        $(this).addClass('direction__slider-big-nav__slide_active').siblings('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active')
        var linkSlide = $(this).attr('data-link'),
            nam = $('.direction__slider-big__slide[data-direction=' + linkSlide+']').index();
        swiperDirection.slideTo(nam, 1000, false);
    });
    swiperDirection.on('slideChange', function () {
        var nameGroup = $('.direction__slider-big__slide').eq(swiperDirection.realIndex).data('direction'),
            selectGroup = $('.direction__slider-big-nav__slide[data-link=' + nameGroup+']');
        swiperDirectionNav.slideTo(selectGroup.index(), 1000, false);
        $('.direction__slider-big-nav__slide[data-link='+ nameGroup +']').addClass('direction__slider-big-nav__slide_active').siblings('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active');
    });    
}
$.faqSlider = function() {
    let faqSliderSelector = $('.faq-slider'); 

    faqSliderSelector.each(function(){
        var mySwiper = new Swiper(this, {
            spaceBetween: 0,
            mousewheel: false,
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                    allowSlideNext: true,
                    allowSlidePrev: true,
                    spaceBetween: 0
                    
                },
                615: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                900: {
                    slidesPerView: 3,
                    allowSlideNext: false,
                    allowSlidePrev: false,
                    spaceBetween: 0
                }
            }
        })
    })
}
var galleryThumbs = new Swiper('.destinations__slider-nav', {
    spaceBetween: 0,
    slideToClickedSlide: true,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
        },
        480: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    },
});
var galleryTop = new Swiper('.destinations__slider', {
    spaceBetween: 10,
    thumbs: {
        swiper: galleryThumbs
    }
});
var swiperDirectionLit = new Swiper('.detailed__slider', {
    spaceBetween: 0,
    slideToClickedSlide: true,
    loopedSlides: 0,
    loop: true,  
    spaceBetween: 35,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
           slidesPerView: 'auto',
           centeredSlides: true,
        },
        480: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    }
});
var detailedSlider = new Swiper('.detailed-description__slider', {
    spaceBetween: 0,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});
var catalogSliderMobile = new Swiper('.catalog__slidr-mobile', {
    
    slidesPerView: 'auto',
    breakpoints: {
        0: {
            slidesPerView: 'auto',
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
            centeredSlides: false,
            slideActiveClass: "catalog__li_active"
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 78,
            centeredSlides: false,
        },
    }
});
$.fancy(
    $('[data-fancybox="image"]')
);
$(document).on('click', '.tabs-link-js:not(.current)', function() {
    $(this).addClass('current').siblings().removeClass('current')
        .parents('.tabs-js').find('.js-tabs__box').eq($(this).index()).fadeIn().siblings('.js-tabs__box').hide();
    return false
});
var originalVal = $.fn.val;
$.fn.val = function(){
    var result =originalVal.apply(this,arguments);
    if(arguments.length>0)
        $(this).change(); // OR with custom event $(this).trigger('value-changed');
    return result;
};
$(document).on('propertychange change click keyup input paste', '.textarea-counter', function(e){
    var max = $(this).attr('maxlength'),
    text_len = $(this).val().length,
    count = max - text_len;
    $(this).closest('.counter__container').find('.counter').html(count);
    originalVal();
});
//validator
$.Valid = function (el) {

    var $el = el;

    if (!$el.length) return;
    var validator = $el.validate({
        rules: {
            name: {
                required: true
            },
            surname: {
                required: true
            },
            password: {
                required: true
            },
            password1: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            password2: {
                required: true,
                minlength: 6,
                equalTo: '[name="password1"]'
            },
            tel: {
                required: true,
                checkMask: true
            },
            check: {
                required: true
            },
            textarea: {
                required: true
            },
            message: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            date: {
                required: true
            },
            submitHandler: function (form) {
                if($(form).hasClass('validLk')){
                    $(form).closest('.lk-page').removeClass('lk-page-edit')
                    $(form).find('.lk-page__input__text').prop('disabled', true);
                    setTimeout(function() {
                        form.submit();
                    }, 2500);
                }else if($(form).hasClass('contacts-form')){
                    var msg   = $(form).serialize(),
                        url = $(form).attr('action');
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: msg,
                        success: function(data) {
                            if(data.result == true || data.result == "ok"){   
                                $('.contacts__box-text').hide();
                                $('.send__ok').show();                                
                            }else {
                                $(form).append('<p class="error-send">' + data.error + '</p>')
                                $('.contacts__box-text').hide();
                                $('.send__error').show();
                            }
                            
                        },
                        error:  function(xhr, str){

                        }
                    });
                }else {
                    form.submit();
                }
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').addClass('error');
            $(element).addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').removeClass('error');
            $(element).removeClass('error');
        }
    })
    $.validator.addMethod("pwcheck", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) 
           && /[a-z]/.test(value) 
           && /\d/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("checkMask", function(value) {
         return $('.phone-masked.ok').length;
    });
};
$('.Valid').each(function(){
    $.Valid($(this));
});
$.fancyText(
    $('[data-fancybox="text"]')
);