//validator
$.Valid = function (el) {

    var $el = el;

    if (!$el.length) return;
    var validator = $el.validate({
        rules: {
            name: {
                required: true
            },
            first_name: {
                required: true
            },
            surname: {
                required: true
            },
            password: {
                required: true
            },
            password1: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            password1_confirmation: {
                required: true,
                minlength: 6,
                equalTo: '[name="password1"]'
            },
            tel: {
                required: true,
                checkMask: true
            },
            phone: {
                required: true,
                checkMask: true
            },
            check: {
                required: true
            },
            textarea: {
                required: true
            },
            message: {
                required: true
            },
            email: {
                required: true,
                mailVal: true
            },
            date: {
                required: true
            },
            from: {
                required: true
            },
            'c[]': {
                required: true
            },
            submitHandler: function (form) {
                if($(form).hasClass('validLk')){
                    $(form).closest('.lk-page').removeClass('lk-page-edit')
                    $(form).find('.lk-page__input__text').prop('disabled', true);
                    setTimeout(function() {
                        form.submit();
                    }, 2500);
                }else if($(form).hasClass('contacts-form')){
                    var msg   = $(form).serialize(),
                        url = $(form).attr('action');
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: msg,
                        success: function(data) {
                            if(data.result == true || data.result == "ok"){   
                                $('.contacts__box-text').hide();
                                $('.send__ok').show();                                
                            }else {
                                $(form).append('<p class="error-send">' + data.error + '</p>')
                                $('.contacts__box-text').hide();
                                $('.send__error').show();
                            }
                            
                        },
                        error:  function(xhr, str){

                        }
                    });
                }else {
                    form.submit();
                }
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').addClass('error');
            $(element).addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').removeClass('error');
            $(element).removeClass('error');
        }
    })
    $.validator.addMethod("pwcheck", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) 
           && /[a-z]/.test(value) 
           && /\d/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("mailVal", function(value) {
       return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("checkMask", function(value) {
         return $('.phone-masked.ok').length;
    });
};
$('.Valid:not(.noval)').each(function(){
    $.Valid($(this));
});
