$.faqCol = function () {
    let a = 0;
    while (a < 3) { 
        $('.faq-slider__swiper-wrapper').append('<div class="swiper-slide faq-slider__swiper-slide"/>');
        a++    
    }
    var sumLinkCol = Math.ceil($('.faq__link-container-box').length / 3);
    var remainsLinkCol = Math.ceil($('.faq__link-container-box').length % 3);
    for (let i = 0; i < sumLinkCol; i++) { 
        
        if(i != sumLinkCol - 1){
            var s = sumLinkCol;
            $('.faq__link-container-box').slice(0, s).prependTo($('.faq-slider__swiper-slide:eq('+ i +')'));  
        } else {
            if(remainsLinkCol == 0) {
                 var s = sumLinkCol;
                 $('.faq__link-container-box').slice(0, s).prependTo($('.faq-slider__swiper-slide:eq('+ i +')')); 
            } else {
                var r = remainsLinkCol;
                $('.faq__link-container-box').slice(0, r).prependTo($('.faq-slider__swiper-slide:eq('+ i +')'));                
            }
        }
    }
}
$(function() {
    $.faqCol();
    $.faqSlider();
});
$('.faq__answer').each(function(){
    var namberEl = $(this).index();
    $(this).clone().addClass('mob').appendTo($(this).closest('.faq__item').find('.faq__question').eq(namberEl))
});
$(document).on('click', '.faq__question-link', function(){
   var namberEl = $(this).closest('.faq__question').index();   $(this).closest('.faq__question').toggleClass('faq__question_active').removeClass('faq__question_siblings').siblings('.faq__question').addClass('faq__question_siblings').removeClass('faq__question_active'); $(this).closest('.faq__item').find('.faq__answer:not(.mob)').eq(namberEl).toggleClass('faq__answer_active').siblings('.faq__answer:not(.mob)').removeClass('faq__answer_active');
   console.log($(this).closest('.faq__item').find('.faq__answer:not(.mob)').eq(namberEl))
    return false
});


$(document).on('click', '.faq__link', function(){
    $('.faq__link').removeClass('active');
    $(this).addClass('active')
    var linkId = $(this).attr('href');
    $(linkId).show(0).siblings('.faq__item').hide(0);
    return false
});
   