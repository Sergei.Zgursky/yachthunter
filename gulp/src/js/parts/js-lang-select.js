$(document).on('click', '.js-select-lang', function(){
    $(this).closest('.header__lang').toggleClass('is-open');
});
$(document).on('click', '.header__lang-hidd-option', function(){
    $(this).addClass('header__lang-hidd-option-select').siblings('.header__lang-hidd-option').removeClass('header__lang-hidd-option-select');
    var text = $(this).text();
    $('.header__lang-select').text(text);
    $(this).closest('.header__lang').removeClass('is-open');
});
$(document).on('click', function(event){
    if( $(event.target).closest('.header__lang').length) 
        return;         
    $('.header__lang').removeClass('is-open');
    event.stopPropagation();
});