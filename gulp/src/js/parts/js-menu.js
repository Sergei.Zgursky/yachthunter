$(document).on('click', '.header__bt-menu', function(){
    $(this).toggleClass('is-active');
    $('.header__control-user').toggleClass('is-active');
    $('html').toggleClass('open-menu');
    if(!$('html').hasClass('open-menu')) {
        $('.main').addClass('transition');
        setTimeout(function() {
            $('.main').removeClass('transition');
        }, 600);
    }
    return false
});
//$('.menu__button').before($('.menu__button').clone().attr('href', '#popup__search').attr('data-modal', 'text').addClass('mob'));