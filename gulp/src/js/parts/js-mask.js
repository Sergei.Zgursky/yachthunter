//Маска телефона
$.masked = function () {
    var $el = $('.phone-masked');
    var maskList = $.masksSort($.masksLoad("json/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			showMaskOnHover: false,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask"
	};
    $el.inputmasks(maskOpts); 
}
if($('.phone-masked').length) {
    $.masked();
}
$(document).on('keyup focusout focusin', '.phone-masked', function() {
    if ($(this).inputmasks('isCompleted')){
        $(this).addClass('ok')
    }else {
        $(this).removeClass('ok')            
    }
});