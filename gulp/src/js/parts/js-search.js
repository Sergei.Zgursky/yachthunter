$(document).on('keyup paste', '.search__input', function(){
    var searchField = $(this).val();
    if(searchField.length >= 2){
        $.ajax({
            url: $(this).closest('form').attr('action') + '/search/'+searchField+'',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                
                var regex = new RegExp(searchField, "i");
                var output = '<div>';
                var count = 1;
                $.each(data, function(key, val){
                    if ((val.title.search(regex) != -1)){
                        output += '<a href="'+ val.link +'" class="search__input-link"><dl class="search__input-link-dl">';
                        output += '<dt class="search__input-link-dt">' + val.title + '</dt>';
                        output += '<dd class="search__input-link-dd">' + val.length + '</dd>'
                        output += '</dl></a>';
                        count++;
                    }
                });
                output += '</div>';
                $('.search__inner .Valid').addClass('noval');
                var validator = $('.noval').validate();
                validator.destroy();
                $('.Valid:not(.noval)').each(function(){
                    $.Valid($(this));
                });
                $('.search__input-result').html(output).addClass('open');
                if($('.search__input-result').find('.search__input-link').length == false) {
                    $('.search__input-result').removeClass('open');
                }
                $(".search__input-link-dt").each(function(){
                    $(this).html($(this).text().replace(
                        new RegExp('(' + $('.search__input').val().replace(/(\[|\^|\$|\/|\\|\+|\*)/g, '\\$1') + ')', 'gi'),
                        '<span>$1</span>'
                    ));        
                });
            }
        })
    }else {
        $('.search__input-result').removeClass('open');
    }
});
$(document).ready(function(){
 
    if($('.search__input').length){
        if($('.search__input').val().length > 1){
            $('.search__inner .Valid').addClass('noval');
            var validator = $('.noval').validate();
            validator.destroy();
            $('.Valid:not(.noval)').each(function(){
                $.Valid($(this));
            });
        }
    }
});
$(document).on('blur', '.search__input', function(event){
    setTimeout(function(){
        $('.search__input-result').removeClass('open');
//        return false
    }, 500)
});