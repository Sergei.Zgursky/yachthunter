$('.detailed__slider__link-tabs').each(function(){
    var numberEl = $(this).closest('.detailed__slider__slide').index();
    $(this).attr('data-link', numberEl);
});
$(document).on('click', '.detailed__slider__link-tabs', function(){
    $('html, body').animate({
        scrollTop: $(".detailed-description__tabs").offset().top - 200
    }, 1000);
    var link = $(this).data('link');
    $('.detailed-description__tabs li').eq(link).trigger('click');
    return false
});
$(document).on('click', '.tabs-link-js:not(.current)', function() {
    $(this).addClass('current').siblings().removeClass('current')
        .parents('.tabs-js').find('.js-tabs__box').eq($(this).index()).fadeIn().siblings('.js-tabs__box').hide();
    return false
});