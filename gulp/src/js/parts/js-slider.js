if($('.direction__slider-big__shadow').length){
    $('.direction__slider-big__shadow').remove();
}
var swiper1 = new Swiper('.slider-direction', {
//    slidesPerView: 'auto',
    spaceBetween: 15,
    slideToClickedSlide: true,
    loop: true,
    centeredSlides: true,
    speed: 600,
    autoplay: {
        delay: 3000,
    },
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
            spaceBetween: 15,
            centeredSlides: true,
        },
        768: {
            slidesPerView: 'auto',
            spaceBetween: 0,
            centeredSlides: true,
        },
        890: {
            slidesPerView: 'auto',
            spaceBetween: 0,
            centeredSlides: true,
        },
        1024: {
            slidesPerView: 3,
            centeredSlides: true,
        },
    }
});

if($('.direction__slider-big__link').length){
//    $('.direction__slider-big__link').hide();
//    if($('.direction__slider-big__link').length == 1){
//        $('.direction__slider-big__link').first().show();
//    }else {
//        $('.direction__slider-big__link').eq(1).show();
//    }
    var loopRezDir = false;
    if($('.direction__slider-big__slide').length > 3){
        loopRezDir = true;
    }
    if($('.direction__slider-big__slide').length > 10 && $(window).width() <= 768){
        loopRezDir = false;
    }
}
var swiper = new Swiper('.direction__slider-big', {
    centeredSlides: true,
    slideToClickedSlide: true,
    initialSlide: 1,
    slidesOffsetBefore: 7,
    loop: true,
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            spaceBetween: 15,
            slidesPerView: 1.4,
            slidesOffsetBefore: 7,
        },
        480: {
            spaceBetween: 15,
            slidesPerView: 2,
        },
        1024: {
            spaceBetween: 103,
            slidesPerView: 2,
        },
        1400: {
            spaceBetween: 103,
            slidesPerView: 3,
        },
        1950: {
            spaceBetween: 103,
            slidesPerView: 3,
        },
        2025: {
            spaceBetween: 103,
            slidesPerView: 4,
        },
    }
});
const swiperSlides = document.getElementsByClassName('direction__slider-big__slide');

var swiperDirectionNav = new Swiper('.direction__slider-big-nav', {
    spaceBetween: 0,
    speed: 600,
    slidesPerView: 'auto',
    loop: true,
    pagination: {
        clickable: true, 
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      
    }
});
if($('.direction__slider-big').length){
    $(document).on('click', '.direction__slider-big-nav__slide', function (e) {
        e.preventDefault();
        $(this).addClass('direction__slider-big-nav__slide_active').siblings('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active');
        var linkSlide = $(this).attr('data-link'),
            nam = $('.direction__slider-big__slide[data-direction=' + linkSlide+']').index();
        swiper.slideTo(nam, 1000, false);
        $('.direction__slider-big-nav__slide[data-link="'+linkSlide+'"]').addClass('direction__slider-big-nav__slide_active');
    });        
    swiper.on('slideChange', function (e) {
        if($('.direction__slider-big-nav').length){
            var nameGroup = $('.direction__slider-big__slide').eq(swiper.activeIndex).data('direction'),
                selectGroup = $('.direction__slider-big-nav__slide[data-link=' + nameGroup+']');
            swiperDirectionNav.slideTo(selectGroup.index(), 1000, false);
            $('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active');
            $('.direction__slider-big-nav__slide[data-link='+ nameGroup +']').addClass('direction__slider-big-nav__slide_active');
            
        }
    });    
}
$.faqSlider = function() {
    let faqSliderSelector = $('.faq-slider'); 

    faqSliderSelector.each(function(){
        var mySwiper = new Swiper(this, {
            spaceBetween: 0,
            mousewheel: false,
            speed: 600,
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                    allowSlideNext: true,
                    allowSlidePrev: true,
                    spaceBetween: 0
                    
                },
                615: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                900: {
                    slidesPerView: 3,
                    allowSlideNext: false,
                    allowSlidePrev: false,
                    spaceBetween: 0
                }
            }
        })
    })
}
var galleryThumbs = new Swiper('.destinations__slider-nav', {
    spaceBetween: 0,
    slideToClickedSlide: true,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
        },
        480: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    },
});
var galleryTop = new Swiper('.destinations__slider', {
    spaceBetween: 70,
    thumbs: {
        swiper: galleryThumbs
    }
});
if($('.detailed__slider').length) {
    var loopRez = false;
    if($('.detailed__slider__slide').length > 4){
        loopRez = true;
    }
    var swiperDirectionLit = new Swiper('.detailed__slider', {
        spaceBetween: 0,
        slideToClickedSlide: true,
        loopedSlides: 0,
        speed: 600,
        spaceBetween: 35,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            0: {
               slidesPerView: 'auto',
               centeredSlides: true,
                loop: false,  
            },
            480: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1024: {
                loop: loopRez,  
                slidesPerView: 4,
            },
        }
    });
}
var detailedSlider = new Swiper('.detailed-description__slider', {
    spaceBetween: 0,
    speed: 600,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});
var catalogSliderMobile = new Swiper('.catalog__slidr-mobile', {
    
    slidesPerView: 'auto',
    speed: 600,
    breakpoints: {
        0: {
            slidesPerView: 'auto',
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
            centeredSlides: false,
            slideActiveClass: "catalog__li_active"
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 77,
            centeredSlides: false,
        },
    }
});
$.fancy(
    $('[data-fancybox="image"]')
);
