if($('.card__info-link_favourites').hasClass('disabled')){
    $('.card__info-link-hidd').empty().text('Чтобы просматривать и распечатывать сохраненные яхты, необходимо зарегистрироваться')
}
$(document).on('click', '.js-save-to-later:not(.catalog__flag)', function() {
    if($('.header__control-user_lk').length){
        $(this).toggleClass('is-active');
        if($(this).hasClass('is-active')){
            $(this).find('.card__info-link-tex').text('В избранном');
            $(this).find('.card__info-link-hidd').show(100);
            setTimeout(function() {
              $('.card__info-link-hidd').hide(100);
            }, 2500);
        }else {
            $(this).find('.card__info-link-tex').text('В избранное');
        }
    }else {
        $(this).find('.card__info-link-hidd').show(100);
        setTimeout(function() {
          $('.card__info-link-hidd').hide(100);
        }, 2500);
        return false
    } 
})
$('.card__detailed').append('<div class="card__detailed_hidd"/>');
//$('.card__price-ok').clone().addClass('card__price-ok_mob').appendTo($('.card__detailed_hidd'));
//$('.card__price').clone().addClass('card__price_mob').appendTo($('.card__detailed_hidd'));

$('.js-link-open-spec').closest('ul').find('li').eq(1).nextAll('li').addClass('hidd_mob');
$(document).on('click', '.js-link-open-spec', function(){
    $(this).closest('ul').toggleClass('open_mob');
    return false
});

$(document).on('click', '.js-card__info-dd-link', function(){
    $(this).closest('li').toggleClass('open_spec');
    return false
});

$(document).ready(function(){
    setTimeout(function(){
        (function ($) {
            $.fn.countTo = function (options) {
                options = options || {};

                return $(this).each(function () {
                    // set options for current element
                    var settings = $.extend({}, $.fn.countTo.defaults, {
                        from:            $(this).data('pricefrom'),
                        to:              $(this).data('priceto'),
                        speed:           $(this).data('speed'),
                        refreshInterval: $(this).data('refresh-interval'),
                        decimals:        $(this).data('decimals')
                    }, options);

                    // how many times to update the value, and how much to increment the value on each update
                    var loops = Math.ceil(settings.speed / settings.refreshInterval),
                        increment = (settings.to - settings.from) / loops;

                    // references & variables that will change with each update
                    var self = this,
                        $self = $(this),
                        loopCount = 0,
                        value = settings.from,
                        data = $self.data('countTo') || {};

                    $self.data('countTo', data);

                    // if an existing interval can be found, clear it first
                    if (data.interval) {
                        clearInterval(data.interval);
                    }
                    data.interval = setInterval(updateTimer, settings.refreshInterval);

                    // initialize the element with the starting value
                    render(value);

                    function updateTimer() {
                        value += increment;
                        loopCount++;

                        render(value);

                        if (typeof(settings.onUpdate) == 'function') {
                            settings.onUpdate.call(self, value);
                        }

                        if (loopCount >= loops) {
                            // remove the interval
                            $self.removeData('countTo');
                            clearInterval(data.interval);
                            value = settings.to;

                            if (typeof(settings.onComplete) == 'function') {
                                settings.onComplete.call(self, value);
                            }
                        }
                    }

                    function render(value) {
                        var formattedValue = settings.formatter.call(self, value, settings);
                        $self.html(formattedValue);
                    }
                });
            };

            $.fn.countTo.defaults = {
                from: 0,               // the number the element should start at
                to: 0,                 // the number the element should end at
                speed: 1000,           // how long it should take to count between the target numbers
                refreshInterval: 100,  // how often the element should be updated
                decimals: 0,           // the number of decimal places to show
                formatter: formatter,  // handler for formatting the value before rendering
                onUpdate: null,        // callback method for every time the element is updated
                onComplete: null       // callback method for when the element finishes updating
            };

            function formatter(value, settings) {
                return value.toFixed(settings.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
            }
        }(jQuery));

        jQuery(function ($) {
          // custom formatting example
            $('.price:not(.price-request)').data('countToOptions', {
                formatter: function (value, options) {
                    return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
                }
            });                
            if($('.card').length){
                var heightPrice = ($('.price:not(.price-request)').offset().top) - ($(window).height()) + 100;
                $(window).scroll(function(){
                    if ($(document).scrollTop() >= heightPrice) {
                        $('.price').each(count);    
                    }          
                })
            };
            if($(document).width() >= 1024){
                if($('.catalog__dd').length){
                    if($('.catalog').hasClass('catalog_tile')){
                        $(document).on('mouseenter', '.catalog__li', function(){
                            if(!$(this).find('.price').hasClass('price-request')){
                                $(this).find('.price:not(.price_complide)').each(function(){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');            
                                });                                 
                            }
                        });
                    }else {
                        if(!$(this).find('.price').hasClass('price-request')){
                            $('.price:not(.price_complide)').each(function(){
                                $(this).each(count);
                                $(this).addClass('price_complide');            
                            });
                        } 
                    }

                };
            }else{
                if($('.catalog__dd').length){
                    $(document).ready(function(){
                        if($('.catalog__li').hasClass('catalog__li_active')){                        
                            $('.catalog__li_active').find('.price:not(.price_complide)').each(function(){
                                if(!$(this).hasClass('price-request')){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');            
                                }
                            }); 
                        }
                    });
                    $(document).on('scroll', function(){
                        if($('.catalog__li').hasClass('catalog__li_active')){                        
                            $('.catalog__li_active').find('.price:not(.price_complide)').each(function(){
                                if(!$(this).hasClass('price-request')){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');    
                                }
                            }); 
                        }
                    });
                }
                
            }
          // start all the timers
            

          function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
              setTimeout(function(){
                $this.countTo(options);  
              }, 1000)
          }
        });
    }, 2500);
})
$(window).scroll(function() {
    var offset_window = $(window).height();
    if($(this).scrollTop() >= offset_window) {
        $('.card__question').addClass('is-visible')
    }else {
        $('.card__question').removeClass('is-visible')
    }
});

if($('.card__button').length) {
    $('.card__button').clone().prependTo($('.header__control'));
}
$btCardFix = function(){
    if($(document).scrollTop() >= $('.first-screen__box .card__button').offset().top) {
        $('.header__control .card__button').addClass('is-visible');
    }else {
        $('.header__control .card__button').removeClass('is-visible');
    }
};
$(document).scroll(function() {
    if($('.header__control .card__button').length){
        $btCardFix()    
    }
})
if($('.card__price-ok_mob-box').length){
    var blockT = document.querySelector('.card__price-ok_mob-box');
        blockT.innerHTML = blockT.innerHTML.replace(/(В НЕДЕЛЮ)/ig, '<span class="normal">$1</span>');
}