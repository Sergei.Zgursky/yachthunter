var windowHeight = $(window).height();
$(window).scroll(function() {
    if($(this).scrollTop() > 10) {
        $('.header').addClass('header__sticky');
    }else {
        $('.header').removeClass('header__sticky');
    }
});

if(!$('.first-screen').length) {
    $('.header').addClass('header_dark');
    $('.main').addClass('main_padd');
}
if($(window).width() <= 1024) {
    $(document).on('click', '.header__control-user_lk', function(){
        $('.header__lk').toggleClass('is-active');
        return false
    });
}
$('.header__control-user:not(.header__control-user_lk)').each(function(){
    $(this).append('<svg class="circle" height="44" stroke="#BE8D55" viewBox="0 0 44 44" width="44"'
                   + 'xmlns="http://www.w3.org/2000/svg">'
                   +    '<g fill-rule="evenodd" fill="none" stroke-width="1">'
                   +      '<circle cx="22" cy="22" r="0"></circle>'
                   +      '<circle cx="22" cy="22" r="0"></circle>'
                   +    '</g>'
                   +    '</svg>')
})
function randomIntFromInterval(min,max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

TweenMax.set('svg', {
  visibility: 'visible'
});

// Loader 8
TweenMax.staggerTo('.circle circle', 2, {
  attr: {
    r: 20
  },
  opacity: 0,
  repeat: -1
}, 1);

function get_name_browser(){
    var ua = navigator.userAgent;
    if (ua.search(/YaBrowser/) > 0) return 'Яндекс Браузер';
    if (ua.search(/rv:11.0/) > 0) return 'Internet Explorer 11';
    if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
    if (ua.search(/Edge/) > 0) return 'Edge';
    if (ua.search(/Chrome/) > 0) return 'Google Chrome';
    if (ua.search(/Firefox/) > 0) return 'Firefox';
    if (ua.search(/Opera/) > 0) return 'Opera';
    if (ua.search(/Safari/) > 0) return 'Safari';
    return 'Не определен';
}

var browser = get_name_browser();
if (browser == 'Edge'|| browser == 'Internet Explorer') {
    $('html').addClass('ov-hidd-x')
} 