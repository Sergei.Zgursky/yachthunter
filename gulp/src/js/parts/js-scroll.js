var swiper = new Swiper('.scroll-container', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    mousewheel: true,
    breakpoints: {
        0: {
            direction: 'horizontal',
        },
        767: {
            direction: 'vertical',
        },
    },
});