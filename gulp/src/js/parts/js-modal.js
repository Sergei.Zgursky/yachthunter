
//popup
$(document).on('click', '[data-modal="text"]', function(e){

    e.preventDefault();
    var src = $(this).attr('href'), 
        bt = $(this);
    $.fancybox.open({
        src: src,
        type: 'inline',
        hash: false,
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#popup__search-main') {
                    $('.fancybox-slide').addClass('event-no')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }
    });    
});


$('.enter__sn').each(function(){
    $(this).closest('.popup__flexbox').find('.submit-box').before($(this).clone().addClass('mob'))  
});
$(document).on('click', '.js-recover', function(){
    $('.enter__sn').hide();
    $('.enter__recover').show();
    $(this).hide();
    return false 
});

$(document).on('click', '.js-popup-link', function(){
    var idLink = $(this).attr('href');
    $(this).closest('.popup').find(idLink).show();
    $(this).closest('.popup__cont').hide();
    return false 
});

$.fancyOptions = {
    type: 'image',
    buttons: [
        "smallBtn"
    ],
    thumbs: {
        autoStart: true,
        axis: 'x',
        hideOnClose: true,    
        parentEl: '.fancybox-container',  
    },
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    hash: false,
    loop: true,
    btnTpl: {
       smallBtn: '<button data-fancybox-close class="fancybox-close-small-img" title="{{CLOSE}}">' +
                    '<span>Закрыть</span>' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
                 '<svg style="width: 1.19rem; height: 0.81rem; transform: rotate(180deg);">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
              '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
                 '<svg style="width: 1.19rem; height: 0.81rem;">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
            '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    },
    afterLoad: function(instance, current) {

        current.$content.closest('.fancybox-container').addClass('photo');
    }
}
$.fancy = function (el) {

    var $el = el;

    if (!$el.length) return;

    $el.attr('data-init', 'true');

    $el.fancybox($.fancyOptions);

};

//$.fancyAuto(
//    $('#popup__search-main')
//);
window.$(document).ready(function() {
    $.fancyAuto = function (el) {
        var $el = el;
        if (!$el.length) return;
    
        $.fancybox.open($el, {

            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }).trigger('click');;
    }
});

