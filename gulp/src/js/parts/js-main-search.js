if ($('.search-main').length) {
    $('.search-main__button').before($('.search-main__button').clone().attr('href', '#popup__search-main').attr('data-modal', 'text').addClass('mob'));
    $('.page').append('<div class="popup popup__search-main" id="popup__search-main"/>');
    $('#popup__search-main').append($('.search-main').clone());
    var $titleMobile = $('.search-main').data('title-mobile');
    $('#popup__search-main').prepend('<p class="popup__title">'+ $titleMobile +'</p>');
    $(".select2-search__field").prop("readonly", true);
}