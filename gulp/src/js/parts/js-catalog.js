var $catalogConstruction = function(el){
    var $el = el;
    var catalogBox = $el.find('.catalog__img-box');
    $el.find('.catalog__dl_price-ok').clone().appendTo(catalogBox);
    $('<div class="catalog__img-box-text"/>').appendTo(catalogBox);
    $el.find('.catalog__dl_price-ok .catalog__dt').append('<span class="lit">(в неделю)</span>');
    var catalogBoxImg = $el.find('.catalog__img-box-text');
    $el.find('.catalog__dl_cabins').clone().appendTo(catalogBoxImg);
    $el.find('.catalog__dl_passengers').clone().appendTo(catalogBoxImg);
    $el.find('.catalog__img-box-text .catalog__dl_cabins .catalog__dd').prepend('<svg class="icon-cabins">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-cabins"></use>' +
                                                                '</svg>')
    $el.find('.catalog__img-box-text .catalog__dl_passengers .catalog__dd').prepend('<svg class="icon-cabins icon-cabins-pass">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-passengers"></use>' +
                                                                '</svg>')
    var yearText = $el.find('.catalog__dl_year .catalog__dd').text();
        yearText = yearText.replace(/\s/g, '');
    $el.find('.catalog__dl_size .catalog__dd').append('<span class="catalog__dl_year-title"> | '+ yearText +'</span>');
    var nospac = $el.find('.catalog__dl_size .catalog__dd').text();   
    $el.find('.catalog__name').wrapInner('<span class="elip"></span>');
    $el.find('.elip span').prepend('<span style="display: none;">/ </span>')
    $el.find('.catalog__dl_size .catalog__dd').empty().text(nospac)
    var catalogName = $el.find('.catalog__name').text();
    $el.find('.catalog__name').attr('data-name', catalogName);  
    $el.addClass('done')
    
    var textDescr = $el.find('.catalog__dl_size').text();
    $el.find('.catalog__dl_size').attr('data-text', textDescr);
}
$('.catalog__li').each(function(){
    $catalogConstruction($(this));
}); 
if($(window).width() > 630 ) {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
    $('.catalog__li:not(.swiper-slide)').eq(1).addClass('catalog__li_active');
}else {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
}
$.fn.catalogActive = function () {
	var windowHeight = $(window).height();
    $('.catalog__li:not(.swiper-slide)').each(function() {
        var $this = $(this),
            height = $this.offset().top + $this.height();
        if($(window).width() > 630 ) {
            if ($(document).scrollTop() + windowHeight + windowHeight/4 >= height && ($(document).scrollTop() + windowHeight + windowHeight/4) - ($this.height() + 65)  <= height ) { 
                $this.addClass('catalog__li_active');
            }else {
                $this.removeClass('catalog__li_active');
            }            
        } else {
            if ($(document).scrollTop() + windowHeight - windowHeight/4 >= height && ($(document).scrollTop() + windowHeight - windowHeight/4) - ($this.height() + 65)  <= height ) { 
                if($('.catalog__li.catalog__li_active').length > 1) {
                    $('.catalog__li.catalog__li_active').first().removeClass('catalog__li_active')
                } 
                if($('.catalog__li.catalog__li_active').length <= 1) {
                    $('.catalog__li:not(.swiper-slide)').removeClass('catalog__li_active'); $this.addClass('catalog__li_active').siblings('.catalog__li_active').removeClass('catalog__li_active');
                }
            }else {
                $this.removeClass('catalog__li_active');
            }
        }
    });    
}
$(document).on('scroll', function(){
    $.fn.catalogActive()
})
$(document).on('load', function(){
    $.fn.catalogActive()
})
if(!$('.header__control-user_lk').length){
    $('.catalog__flag-hide').addClass('catalog__flag-hide_no').empty().text('Чтобы просматривать и распечатывать сохраненные яхты, необходимо зарегистрироваться')
}
$(document).on('click', '.catalog__flag', function(e) {
    if($('.header__control-user_lk').length){
        $(this).toggleClass('is-active');
        if($(this).hasClass('is-active')){
            $(this).find('.catalog__flag-hide').show(100);
            setTimeout(function() {
              $('.catalog__flag-hide').hide(100);
            }, 2500);
        }        
    }else {
        e.stopPropagation();
        $(this).find('.catalog__flag-hide').show(100);
        $(this).addClass('no-event');
        setTimeout(function() {
          $('.catalog__flag-hide').hide(100);
          $(this).removeClass('no-event');
        }, 2500);
        return false
    }
    
})
    
$(document).on('click', '.js-control', function() {
    $(this).addClass('is-active').siblings('.js-control').removeClass('is-active');
    if($(this).hasClass('tile')){
        $('.catalog').addClass('catalog_tile');
    } else {
        $('.catalog').removeClass('catalog_tile');
    }
    $.catalogLoad();
    return false
});

if ($('.page__search .catalog').length) {
    $(document).on('load resize', function(){
        
    })
}

$(document).on('click', '.catalog__li', function() {
    $(this).addClass('viewed');
});
$.catalogLoad = function() {
    var load_more = false;

    var currentPage = 1;
    var baseUrl = window.location.href;
    if(baseUrl.indexOf('page')>= 0){
        var namberPage = parseInt(baseUrl.match(/page=(\d+)/)[1]);
        currentPage = namberPage;
        baseUrl = baseUrl.replace('&page=' + namberPage, '');
        history.pushState(null, null, baseUrl); 
    }
    $(window).scroll(function() {
        if($(".prelod-page:not(.ok)").length && !load_more) {
			if($('.catalog').length) {
            	var offset_button = $(".catalog li").last().offset();
			}
			if($('.destinations__slider').length) {
            	var offset_button = $(".destinations__slider__link").last().offset();
				var directions = '/' + $('.swiper-slide-thumb-active').data('link');
			}
            if($(this).scrollTop() >= offset_button.top - $(window).height()) {
                currentPage++;
                var r = Math.random();
                if(baseUrl.indexOf('?')== -1){
                    var numberPage = '?&page='
                }else {
                    var numberPage = '&page=';
                }
				if($('.catalog').length) {
					var  newUrl = baseUrl + numberPage + currentPage;
					history.pushState(null, null, newUrl);                   
					var sendUrl = baseUrl + numberPage + currentPage + '&' + r;
				}
				if($('.destinations__slider').length) {
					var  newUrl = baseUrl + directions + numberPage + currentPage;
					
					var sendUrl = baseUrl + directions + numberPage + currentPage + '&' + r;
				}
                load_more = true;
                $('.prelod-page').show();
                if(!$(".bt-top").length){
                    if(currentPage >= 3) {
                       $('body').append('<a href="#" class="bt-top"/>') 
                    }
                }
                $.ajax({
                    url: sendUrl,
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if(!(response == "")) {
                            if(response.indexOf('catalog') > 0) {
                                if($('.catalog').length) {
                                    $('.catalog__list').append(response);
                                    $('.catalog__li').each(function(){
                                        if(!$(this).hasClass('done')){
                                            $catalogConstruction($(this));
                                        }
                                    }); 
                                }
                                if($('.destinations__slider').length) {
                                    $('.swiper-slide-active .destinations__slider__container').append(response);
                                }
                                $('.prelod-page').hide();
                                load_more = false;                                 
                            }else {                             
                                $('.prelod-page').hide();
                                return false
                                load_more = false;
                            }
                        }
                        if(response == "") {
                            $('.prelod-page').addClass('ok').hide();
                            load_more = false;
                            currentPage--
                            var  newUrl = baseUrl + numberPage + currentPage;
							if($('.catalog').length) {
								history.pushState(null, null, newUrl);
							}
                            return false
                        }
                    },
                    error: function(response) {
                        $('.prelod-page').hide();
                        return false
                        load_more = false;
                    }
                });
            }
        }
    });    
};
$(window).scroll(function () {
    
    if ($(window).scrollTop()  == 0) {
        $('.bt-top').hide();
    } else {
        $('.bt-top').show();
    }
});
$(document).on('click', '.bt-top', function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
    return false
})
$.catalogLoad();

var cookieValueHint = document.cookie.replace(/(?:(?:^|.*;\s*)hint\s*\=\s*([^;]*).*$)|^.*$/, "$1");
$(document).ready(function(){
    setTimeout(function(){
        if($('.catalog').length){
            if($('.search__result').length){
                $('html').animate({scrollTop: $('.search__result').offset().top -120}, 500);
                if(cookieValueHint != 'false'){
                    if(!$('.header__control-user').hasClass('header__control-user_lk')) {
                        if($(window).width() <= 1024){
                            $('.header__control-user-text-hidd').addClass('is-active');
                            document.cookie = "hint=false";
                            setTimeout(function(){
                                $('.header__control-user-text-hidd').removeClass('is-active');
                            }, 5000);
                        }                    
                    }
                }
            }
        }     
        
    }, 500)
});