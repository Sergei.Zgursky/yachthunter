
$.fn.parallax = function (resistance, mouse) {
    $el = $(this);
    $elContainer = $el.parent();
    TweenLite.to($el, 0.1, {
        x: -((mouse.clientX - $elContainer.width() * 1.5) / resistance)*1.5,
        y: -((mouse.clientY - $elContainer.height() * 1.5) / resistance)*1.5,
        ease: Power1.easeInOut
    });
};
$(document).on('mousemove', '.js-parallax-container', function(e) {
    $(this).find(".js-parallax").parallax(-80, e);
});


