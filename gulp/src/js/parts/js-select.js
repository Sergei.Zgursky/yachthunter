$.select = function(el) {
    var $el = el;

    if (!$el.length) return;
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
    
    var placeholder = $el.data('placeholder'); 
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: true,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        dropdownParent: dropdownParent,
        width: '100%',
        dropdownPosition: 'below'
    });
}
$.selectMulte = function(el) {
    var $el = el;

    if (!$el.length) return;
    
    var placeholder = $el.attr('placeholder'); 
    
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
  
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: false,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        width: '100%',
        closeOnSelect: false,
        dropdownParent: dropdownParent,
        dropdownPosition: 'below',
        "language": {
           "noResults": function(){
               return "Ничего не найдено";
           }
       },
    });
    $el.each(function(){
        if($(this).attr('name') == 'c[]'){
            $(this).addClass('selectDirection').closest('.search-main__box-select').addClass('selectDirection__box');
            $(this).addClass('selectDirection').closest('.search__select-container').addClass('selectDirection__box');
            if($('html').hasClass('no-touchevents')){
                $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", false);
            }else {
                $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", true);
            }
        }else {
            $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", true);
        }
    });
}


$('.multiple').on('select2:selecting', function(e) {
    var id = e.params.args.data.element.dataset.allselect;
    if(id == 'true') {
        $(this).find('option:not([data-allselect])').prop('selected', false);
    }else{
        $(this).find('option[data-allselect]').prop('selected', false);
    }
});
$('.multiple').on('select2:selecting', function(e) {
    $(this).closest('div').find('.select2-search__field').val(' ');
})
if($('.no-touchevents').length){
    $(document).on('mouseenter', '.tooltip-ok', function() {
        $('.tooltip-hide').addClass('is-active');
    });
    $(document).on('mouseleave', '.tooltip-ok', function() {
        $('.tooltip-hide').removeClass('is-active');
    });
};
if($('.touchevents').length){
    $(document).on('click', '.tooltip', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('.multiple').select2("close");
        $('.tooltip-hide').toggleClass('is-active');
        return false
    });
};
$(document).on('click', function(event){
    if( $(event.target).closest('.selectDirection').length) 
        return;         
    $('.tooltip').removeClass('is-active');
    event.stopPropagation();
});
$.selectDirectionTooltip = function(el) {
    $('.selectDirection__box').addClass('tooltip-ok');
    $('.tooltip').remove();
    $('.tooltip-hide').remove();
    $('.selectDirection__box .select2-search').before('<li class="select2-selection__choice dropdown-toggle tooltip" href="javascript:;" data-toggle="dropdown">...</li>');
    if($('.search-main').length){
        $('.search-main').append('<div class="tooltip-hide"/>');
    }
    if($('.search__select-container').length){
        $('.search__select-container.selectDirection__box').append('<div class="tooltip-hide"/>');
    }
    var tooltipText = $('.selectDirection__box .select2-selection__choice:not(.tooltip)').text();
        tooltipText = tooltipText.replace('×', '');
        tooltipText = tooltipText.replace(/[×]/g, ", ");
    $('.tooltip-hide').text(tooltipText);
}
$('.multiple').on('select2:closing', function(e) {
    if($('.selectDirection').find('option:selected').length > 2 ){
        $.selectDirectionTooltip();
    }
 
    if($('.selectSize').length && $('.selectPrice').length) {
        
        if($(this).hasClass('selectSize')) {
            if($(this).find('option:selected').length > 0){
                if($(this).find('option[data-allselect]:selected').length !=1){
                    $('.selectPrice').find('option:not([data-allselect])').prop('disabled', true);
                    $(this).find('option:selected').each(function(){
                        var allLongMin = $(this).data('longfrom'),
                            allLongMax = $(this).data('longto');
                        if(allLongMin != null) {
                            $('.selectPrice option').each(function(){
                                if($(this).attr('data-longfrom') == allLongMin){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }
                        if(allLongMax != null) {
                            $('.selectPrice option').each(function(){
                                if($(this).attr('data-longto') == allLongMax){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }                    
                    });     
                }else {
                    $('.selectPrice').find('option').prop('disabled', false);
                }
            }else {
                $('.selectPrice').find('option').prop('disabled', false);
            }
        }
        if($(this).hasClass('selectPrice')) {
            if($(this).find('option:selected').length > 0){
                if($(this).find('option[data-allselect]:selected').length !=1){
                    $('.selectSize').find('option:not([data-allselect])').prop('disabled', true);
                    $(this).find('option:selected').each(function(){

                        var allLongMin = $(this).data('longfrom'),
                            allLongMax = $(this).data('longto');
                        if(allLongMin != null) {
                            $('.selectSize option').each(function(){
                                if($(this).attr('data-longfrom') == allLongMin){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }
                        if(allLongMax != null) {
                            $('.selectSize option').each(function(){
                                if($(this).attr('data-longto') == allLongMax){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }                    
                    });     
                }else {
                    $('.selectSize').find('option').prop('disabled', false);
                }
            }else {
                $('.selectSize').find('option').prop('disabled', false);
            }
        }
    }    
})

$('.multiple').each(function(){
    $.selectMulte($(this));
});
$('.select').each(function(){
    $.select($(this));
});