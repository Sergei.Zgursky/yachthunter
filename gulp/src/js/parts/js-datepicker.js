
$.datepickerOp = function(){
    var $el = $('.datepicker-input:not(.openDat)');
    $el.datepicker({
        range: true,
        multipleDatesSeparator: '-',
        minDate: new Date(),
        autoClose: true,
        onShow: function(dp, animationCompleted){
            if (animationCompleted) {
                $el.addClass('openDat');  
            }
        },
        onHide: function(dp, animationCompleted){
            $el.removeClass('openDat');            
        }
    })    
};
$.datepickerOp();
if($('.datepicker-input_lighten').length){
    $('.datepickers-container').addClass('datepickers-container_lighten')
}
