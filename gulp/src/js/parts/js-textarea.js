var originalVal = $.fn.val;
$.fn.val = function(){
    var result =originalVal.apply(this,arguments);
    if(arguments.length>0)
        $(this).change(); // OR with custom event $(this).trigger('value-changed');
    return result;
};
$(document).on('propertychange change click keyup input paste', '.textarea-counter', function(e){
    var max = $(this).attr('maxlength'),
    text_len = $(this).val().length,
    count = max - text_len;
    $(this).closest('.counter__container').find('.counter').html(count);
    originalVal();
});