$(document).ready(function(){
    $('.preloader__line').on('animationend', function(){
        $('.preloader').addClass('complide');
        setTimeout(function(){
            $('.preloader__img-f').addClass('active');
        }, 800);
        setTimeout(function(){
            $('.preloader').hide(0);
        }, 1500);
    });
});
setTimeout(function(){
    if(!$('.preloader').hasClass('complide')) {
        $('.preloader').addClass('complide');
        setTimeout(function(){
            $('.preloader__img-f').addClass('active');
        }, 800);
        setTimeout(function(){
            $('.preloader').hide(0);
        }, 1500);
    }
}, 2500); 
$(document).on('click', '.js-anhor', function(){
    var target = $(this).attr('href');
    $('html, body').animate({scrollTop: $(target).offset().top}, 1000);
    return false;
})
if($('.card__info-link_favourites').hasClass('disabled')){
    $('.card__info-link-hidd').empty().text('Чтобы просматривать и распечатывать сохраненные яхты, необходимо зарегистрироваться')
}
$(document).on('click', '.js-save-to-later:not(.catalog__flag)', function() {
    if($('.header__control-user_lk').length){
        $(this).toggleClass('is-active');
        if($(this).hasClass('is-active')){
            $(this).find('.card__info-link-tex').text('В избранном');
            $(this).find('.card__info-link-hidd').show(100);
            setTimeout(function() {
              $('.card__info-link-hidd').hide(100);
            }, 2500);
        }else {
            $(this).find('.card__info-link-tex').text('В избранное');
        }
    }else {
        $(this).find('.card__info-link-hidd').show(100);
        setTimeout(function() {
          $('.card__info-link-hidd').hide(100);
        }, 2500);
        return false
    } 
})
$('.card__detailed').append('<div class="card__detailed_hidd"/>');
//$('.card__price-ok').clone().addClass('card__price-ok_mob').appendTo($('.card__detailed_hidd'));
//$('.card__price').clone().addClass('card__price_mob').appendTo($('.card__detailed_hidd'));

$('.js-link-open-spec').closest('ul').find('li').eq(1).nextAll('li').addClass('hidd_mob');
$(document).on('click', '.js-link-open-spec', function(){
    $(this).closest('ul').toggleClass('open_mob');
    return false
});

$(document).on('click', '.js-card__info-dd-link', function(){
    $(this).closest('li').toggleClass('open_spec');
    return false
});

$(document).ready(function(){
    setTimeout(function(){
        (function ($) {
            $.fn.countTo = function (options) {
                options = options || {};

                return $(this).each(function () {
                    // set options for current element
                    var settings = $.extend({}, $.fn.countTo.defaults, {
                        from:            $(this).data('pricefrom'),
                        to:              $(this).data('priceto'),
                        speed:           $(this).data('speed'),
                        refreshInterval: $(this).data('refresh-interval'),
                        decimals:        $(this).data('decimals')
                    }, options);

                    // how many times to update the value, and how much to increment the value on each update
                    var loops = Math.ceil(settings.speed / settings.refreshInterval),
                        increment = (settings.to - settings.from) / loops;

                    // references & variables that will change with each update
                    var self = this,
                        $self = $(this),
                        loopCount = 0,
                        value = settings.from,
                        data = $self.data('countTo') || {};

                    $self.data('countTo', data);

                    // if an existing interval can be found, clear it first
                    if (data.interval) {
                        clearInterval(data.interval);
                    }
                    data.interval = setInterval(updateTimer, settings.refreshInterval);

                    // initialize the element with the starting value
                    render(value);

                    function updateTimer() {
                        value += increment;
                        loopCount++;

                        render(value);

                        if (typeof(settings.onUpdate) == 'function') {
                            settings.onUpdate.call(self, value);
                        }

                        if (loopCount >= loops) {
                            // remove the interval
                            $self.removeData('countTo');
                            clearInterval(data.interval);
                            value = settings.to;

                            if (typeof(settings.onComplete) == 'function') {
                                settings.onComplete.call(self, value);
                            }
                        }
                    }

                    function render(value) {
                        var formattedValue = settings.formatter.call(self, value, settings);
                        $self.html(formattedValue);
                    }
                });
            };

            $.fn.countTo.defaults = {
                from: 0,               // the number the element should start at
                to: 0,                 // the number the element should end at
                speed: 1000,           // how long it should take to count between the target numbers
                refreshInterval: 100,  // how often the element should be updated
                decimals: 0,           // the number of decimal places to show
                formatter: formatter,  // handler for formatting the value before rendering
                onUpdate: null,        // callback method for every time the element is updated
                onComplete: null       // callback method for when the element finishes updating
            };

            function formatter(value, settings) {
                return value.toFixed(settings.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
            }
        }(jQuery));

        jQuery(function ($) {
          // custom formatting example
            $('.price:not(.price-request)').data('countToOptions', {
                formatter: function (value, options) {
                    return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
                }
            });                
            if($('.card').length){
                var heightPrice = ($('.price:not(.price-request)').offset().top) - ($(window).height()) + 100;
                $(window).scroll(function(){
                    if ($(document).scrollTop() >= heightPrice) {
                        $('.price').each(count);    
                    }          
                })
            };
            if($(document).width() >= 1024){
                if($('.catalog__dd').length){
                    if($('.catalog').hasClass('catalog_tile')){
                        $(document).on('mouseenter', '.catalog__li', function(){
                            if(!$(this).find('.price').hasClass('price-request')){
                                $(this).find('.price:not(.price_complide)').each(function(){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');            
                                });                                 
                            }
                        });
                    }else {
                        if(!$(this).find('.price').hasClass('price-request')){
                            $('.price:not(.price_complide)').each(function(){
                                $(this).each(count);
                                $(this).addClass('price_complide');            
                            });
                        } 
                    }

                };
            }else{
                if($('.catalog__dd').length){
                    $(document).ready(function(){
                        if($('.catalog__li').hasClass('catalog__li_active')){                        
                            $('.catalog__li_active').find('.price:not(.price_complide)').each(function(){
                                if(!$(this).hasClass('price-request')){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');            
                                }
                            }); 
                        }
                    });
                    $(document).on('scroll', function(){
                        if($('.catalog__li').hasClass('catalog__li_active')){                        
                            $('.catalog__li_active').find('.price:not(.price_complide)').each(function(){
                                if(!$(this).hasClass('price-request')){
                                    $(this).each(count);
                                    $(this).addClass('price_complide');    
                                }
                            }); 
                        }
                    });
                }
                
            }
          // start all the timers
            

          function count(options) {
            var $this = $(this);
            options = $.extend({}, options || {}, $this.data('countToOptions') || {});
              setTimeout(function(){
                $this.countTo(options);  
              }, 1000)
          }
        });
    }, 2500);
})
$(window).scroll(function() {
    var offset_window = $(window).height();
    if($(this).scrollTop() >= offset_window) {
        $('.card__question').addClass('is-visible')
    }else {
        $('.card__question').removeClass('is-visible')
    }
});

if($('.card__button').length) {
    $('.card__button').clone().prependTo($('.header__control'));
}
$btCardFix = function(){
    if($(document).scrollTop() >= $('.first-screen__box .card__button').offset().top) {
        $('.header__control .card__button').addClass('is-visible');
    }else {
        $('.header__control .card__button').removeClass('is-visible');
    }
};
$(document).scroll(function() {
    if($('.header__control .card__button').length){
        $btCardFix()    
    }
})
if($('.card__price-ok_mob-box').length){
    var blockT = document.querySelector('.card__price-ok_mob-box');
        blockT.innerHTML = blockT.innerHTML.replace(/(В НЕДЕЛЮ)/ig, '<span class="normal">$1</span>');
}
var $catalogConstruction = function(el){
    var $el = el;
    var catalogBox = $el.find('.catalog__img-box');
    $el.find('.catalog__dl_price-ok').clone().appendTo(catalogBox);
    $('<div class="catalog__img-box-text"/>').appendTo(catalogBox);
    $el.find('.catalog__dl_price-ok .catalog__dt').append('<span class="lit">(в неделю)</span>');
    var catalogBoxImg = $el.find('.catalog__img-box-text');
    $el.find('.catalog__dl_cabins').clone().appendTo(catalogBoxImg);
    $el.find('.catalog__dl_passengers').clone().appendTo(catalogBoxImg);
    $el.find('.catalog__img-box-text .catalog__dl_cabins .catalog__dd').prepend('<svg class="icon-cabins">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-cabins"></use>' +
                                                                '</svg>')
    $el.find('.catalog__img-box-text .catalog__dl_passengers .catalog__dd').prepend('<svg class="icon-cabins icon-cabins-pass">' +
                                                                '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-passengers"></use>' +
                                                                '</svg>')
    var yearText = $el.find('.catalog__dl_year .catalog__dd').text();
        yearText = yearText.replace(/\s/g, '');
    $el.find('.catalog__dl_size .catalog__dd').append('<span class="catalog__dl_year-title"> | '+ yearText +'</span>');
    var nospac = $el.find('.catalog__dl_size .catalog__dd').text();   
    $el.find('.catalog__name').wrapInner('<span class="elip"></span>');
    $el.find('.elip span').prepend('<span style="display: none;">/ </span>')
    $el.find('.catalog__dl_size .catalog__dd').empty().text(nospac)
    var catalogName = $el.find('.catalog__name').text();
    $el.find('.catalog__name').attr('data-name', catalogName);  
    $el.addClass('done')
    
    var textDescr = $el.find('.catalog__dl_size').text();
    $el.find('.catalog__dl_size').attr('data-text', textDescr);
}
$('.catalog__li').each(function(){
    $catalogConstruction($(this));
}); 
if($(window).width() > 630 ) {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
    $('.catalog__li:not(.swiper-slide)').eq(1).addClass('catalog__li_active');
}else {
    $('.catalog__li:not(.swiper-slide)').eq(0).addClass('catalog__li_active');
}
$.fn.catalogActive = function () {
	var windowHeight = $(window).height();
    $('.catalog__li:not(.swiper-slide)').each(function() {
        var $this = $(this),
            height = $this.offset().top + $this.height();
        if($(window).width() > 630 ) {
            if ($(document).scrollTop() + windowHeight + windowHeight/4 >= height && ($(document).scrollTop() + windowHeight + windowHeight/4) - ($this.height() + 65)  <= height ) { 
                $this.addClass('catalog__li_active');
            }else {
                $this.removeClass('catalog__li_active');
            }            
        } else {
            if ($(document).scrollTop() + windowHeight - windowHeight/4 >= height && ($(document).scrollTop() + windowHeight - windowHeight/4) - ($this.height() + 65)  <= height ) { 
                if($('.catalog__li.catalog__li_active').length > 1) {
                    $('.catalog__li.catalog__li_active').first().removeClass('catalog__li_active')
                } 
                if($('.catalog__li.catalog__li_active').length <= 1) {
                    $('.catalog__li:not(.swiper-slide)').removeClass('catalog__li_active'); $this.addClass('catalog__li_active').siblings('.catalog__li_active').removeClass('catalog__li_active');
                }
            }else {
                $this.removeClass('catalog__li_active');
            }
        }
    });    
}
$(document).on('scroll', function(){
    $.fn.catalogActive()
})
$(document).on('load', function(){
    $.fn.catalogActive()
})
if(!$('.header__control-user_lk').length){
    $('.catalog__flag-hide').addClass('catalog__flag-hide_no').empty().text('Чтобы просматривать и распечатывать сохраненные яхты, необходимо зарегистрироваться')
}
$(document).on('click', '.catalog__flag', function(e) {
    if($('.header__control-user_lk').length){
        $(this).toggleClass('is-active');
        if($(this).hasClass('is-active')){
            $(this).find('.catalog__flag-hide').show(100);
            setTimeout(function() {
              $('.catalog__flag-hide').hide(100);
            }, 2500);
        }        
    }else {
        e.stopPropagation();
        $(this).find('.catalog__flag-hide').show(100);
        $(this).addClass('no-event');
        setTimeout(function() {
          $('.catalog__flag-hide').hide(100);
          $(this).removeClass('no-event');
        }, 2500);
        return false
    }
    
})
    
$(document).on('click', '.js-control', function() {
    $(this).addClass('is-active').siblings('.js-control').removeClass('is-active');
    if($(this).hasClass('tile')){
        $('.catalog').addClass('catalog_tile');
    } else {
        $('.catalog').removeClass('catalog_tile');
    }
    $.catalogLoad();
    return false
});

if ($('.page__search .catalog').length) {
    $(document).on('load resize', function(){
        
    })
}

$(document).on('click', '.catalog__li', function() {
    $(this).addClass('viewed');
});
$.catalogLoad = function() {
    var load_more = false;

    var currentPage = 1;
    var baseUrl = window.location.href;
    if(baseUrl.indexOf('page')>= 0){
        var namberPage = parseInt(baseUrl.match(/page=(\d+)/)[1]);
        currentPage = namberPage;
        baseUrl = baseUrl.replace('&page=' + namberPage, '');
        history.pushState(null, null, baseUrl); 
    }
    $(window).scroll(function() {
        if($(".prelod-page:not(.ok)").length && !load_more) {
			if($('.catalog').length) {
            	var offset_button = $(".catalog li").last().offset();
			}
			if($('.destinations__slider').length) {
            	var offset_button = $(".destinations__slider__link").last().offset();
				var directions = '/' + $('.swiper-slide-thumb-active').data('link');
			}
            if($(this).scrollTop() >= offset_button.top - $(window).height()) {
                currentPage++;
                var r = Math.random();
                if(baseUrl.indexOf('?')== -1){
                    var numberPage = '?&page='
                }else {
                    var numberPage = '&page=';
                }
				if($('.catalog').length) {
					var  newUrl = baseUrl + numberPage + currentPage;
					history.pushState(null, null, newUrl);                   
					var sendUrl = baseUrl + numberPage + currentPage + '&' + r;
				}
				if($('.destinations__slider').length) {
					var  newUrl = baseUrl + directions + numberPage + currentPage;
					
					var sendUrl = baseUrl + directions + numberPage + currentPage + '&' + r;
				}
                load_more = true;
                $('.prelod-page').show();
                if(!$(".bt-top").length){
                    if(currentPage >= 3) {
                       $('body').append('<a href="#" class="bt-top"/>') 
                    }
                }
                $.ajax({
                    url: sendUrl,
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if(!(response == "")) {
                            if(response.indexOf('catalog') > 0) {
                                if($('.catalog').length) {
                                    $('.catalog__list').append(response);
                                    $('.catalog__li').each(function(){
                                        if(!$(this).hasClass('done')){
                                            $catalogConstruction($(this));
                                        }
                                    }); 
                                }
                                if($('.destinations__slider').length) {
                                    $('.swiper-slide-active .destinations__slider__container').append(response);
                                }
                                $('.prelod-page').hide();
                                load_more = false;                                 
                            }else {                             
                                $('.prelod-page').hide();
                                return false
                                load_more = false;
                            }
                        }
                        if(response == "") {
                            $('.prelod-page').addClass('ok').hide();
                            load_more = false;
                            currentPage--
                            var  newUrl = baseUrl + numberPage + currentPage;
							if($('.catalog').length) {
								history.pushState(null, null, newUrl);
							}
                            return false
                        }
                    },
                    error: function(response) {
                        $('.prelod-page').hide();
                        return false
                        load_more = false;
                    }
                });
            }
        }
    });    
};
$(window).scroll(function () {
    
    if ($(window).scrollTop()  == 0) {
        $('.bt-top').hide();
    } else {
        $('.bt-top').show();
    }
});
$(document).on('click', '.bt-top', function(){
    $('body,html').animate({
        scrollTop: 0
    }, 500);
    return false
})
$.catalogLoad();

var cookieValueHint = document.cookie.replace(/(?:(?:^|.*;\s*)hint\s*\=\s*([^;]*).*$)|^.*$/, "$1");
$(document).ready(function(){
    setTimeout(function(){
        if($('.catalog').length){
            if($('.search__result').length){
                $('html').animate({scrollTop: $('.search__result').offset().top -120}, 500);
                if(cookieValueHint != 'false'){
                    if(!$('.header__control-user').hasClass('header__control-user_lk')) {
                        if($(window).width() <= 1024){
                            $('.header__control-user-text-hidd').addClass('is-active');
                            document.cookie = "hint=false";
                            setTimeout(function(){
                                $('.header__control-user-text-hidd').removeClass('is-active');
                            }, 5000);
                        }                    
                    }
                }
            }
        }     
        
    }, 500)
});
var cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)cookie1\s*\=\s*([^;]*).*$)|^.*$/, "$1");
if(cookieValue != 'close'){
    $('.cookie').addClass('visible')
}

$(document).on('click', '.cookie__close', function(){
    $(this).closest('.cookie').remove();
    document.cookie = "cookie1=close";
    return false
});

$.datepickerOp = function(){
    var $el = $('.datepicker-input:not(.openDat)');
    $el.datepicker({
        range: true,
        multipleDatesSeparator: '-',
        minDate: new Date(),
        autoClose: true,
        onShow: function(dp, animationCompleted){
            if (animationCompleted) {
                $el.addClass('openDat');  
            }
        },
        onHide: function(dp, animationCompleted){
            $el.removeClass('openDat');            
        }
    })    
};
$.datepickerOp();
if($('.datepicker-input_lighten').length){
    $('.datepickers-container').addClass('datepickers-container_lighten')
}

$.faqCol = function () {
    let a = 0;
    while (a < 3) { 
        $('.faq-slider__swiper-wrapper').append('<div class="swiper-slide faq-slider__swiper-slide"/>');
        a++    
    }
    var sumLinkCol = Math.ceil($('.faq__link-container-box').length / 3);
    var remainsLinkCol = Math.ceil($('.faq__link-container-box').length % 3);
    for (let i = 0; i < sumLinkCol; i++) { 
        
        if(i != sumLinkCol - 1){
            var s = sumLinkCol;
            $('.faq__link-container-box').slice(0, s).prependTo($('.faq-slider__swiper-slide:eq('+ i +')'));  
        } else {
            if(remainsLinkCol == 0) {
                 var s = sumLinkCol;
                 $('.faq__link-container-box').slice(0, s).prependTo($('.faq-slider__swiper-slide:eq('+ i +')')); 
            } else {
                var r = remainsLinkCol;
                $('.faq__link-container-box').slice(0, r).prependTo($('.faq-slider__swiper-slide:eq('+ i +')'));                
            }
        }
    }
}
$(function() {
    $.faqCol();
    $.faqSlider();
});
$('.faq__answer').each(function(){
    var namberEl = $(this).index();
    $(this).clone().addClass('mob').appendTo($(this).closest('.faq__item').find('.faq__question').eq(namberEl))
});
$(document).on('click', '.faq__question-link', function(){
   var namberEl = $(this).closest('.faq__question').index();   $(this).closest('.faq__question').toggleClass('faq__question_active').removeClass('faq__question_siblings').siblings('.faq__question').addClass('faq__question_siblings').removeClass('faq__question_active'); $(this).closest('.faq__item').find('.faq__answer:not(.mob)').eq(namberEl).toggleClass('faq__answer_active').siblings('.faq__answer:not(.mob)').removeClass('faq__answer_active');
   console.log($(this).closest('.faq__item').find('.faq__answer:not(.mob)').eq(namberEl))
    return false
});


$(document).on('click', '.faq__link', function(){
    $('.faq__link').removeClass('active');
    $(this).addClass('active')
    var linkId = $(this).attr('href');
    $(linkId).show(0).siblings('.faq__item').hide(0);
    return false
});
   
if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    $('html').addClass('ios');
}
var windowHeight = $(window).height();
$(window).scroll(function() {
    if($(this).scrollTop() > 10) {
        $('.header').addClass('header__sticky');
    }else {
        $('.header').removeClass('header__sticky');
    }
});

if(!$('.first-screen').length) {
    $('.header').addClass('header_dark');
    $('.main').addClass('main_padd');
}
if($(window).width() <= 1024) {
    $(document).on('click', '.header__control-user_lk', function(){
        $('.header__lk').toggleClass('is-active');
        return false
    });
}
$('.header__control-user:not(.header__control-user_lk)').each(function(){
    $(this).append('<svg class="circle" height="44" stroke="#BE8D55" viewBox="0 0 44 44" width="44"'
                   + 'xmlns="http://www.w3.org/2000/svg">'
                   +    '<g fill-rule="evenodd" fill="none" stroke-width="1">'
                   +      '<circle cx="22" cy="22" r="0"></circle>'
                   +      '<circle cx="22" cy="22" r="0"></circle>'
                   +    '</g>'
                   +    '</svg>')
})
function randomIntFromInterval(min,max) {
  return Math.floor(Math.random()*(max-min+1)+min);
}

TweenMax.set('svg', {
  visibility: 'visible'
});

// Loader 8
TweenMax.staggerTo('.circle circle', 2, {
  attr: {
    r: 20
  },
  opacity: 0,
  repeat: -1
}, 1);

function get_name_browser(){
    var ua = navigator.userAgent;
    if (ua.search(/YaBrowser/) > 0) return 'Яндекс Браузер';
    if (ua.search(/rv:11.0/) > 0) return 'Internet Explorer 11';
    if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
    if (ua.search(/Edge/) > 0) return 'Edge';
    if (ua.search(/Chrome/) > 0) return 'Google Chrome';
    if (ua.search(/Firefox/) > 0) return 'Firefox';
    if (ua.search(/Opera/) > 0) return 'Opera';
    if (ua.search(/Safari/) > 0) return 'Safari';
    return 'Не определен';
}

var browser = get_name_browser();
if (browser == 'Edge'|| browser == 'Internet Explorer') {
    $('html').addClass('ov-hidd-x')
} 
document.querySelector("#popup__enter .input__text").oninput = function(){
  this.value = this.value.replace(/[()+]/i, '');
}
$(document).on('click', '.js-select-lang', function(){
    $(this).closest('.header__lang').toggleClass('is-open');
});
$(document).on('click', '.header__lang-hidd-option', function(){
    $(this).addClass('header__lang-hidd-option-select').siblings('.header__lang-hidd-option').removeClass('header__lang-hidd-option-select');
    var text = $(this).text();
    $('.header__lang-select').text(text);
    $(this).closest('.header__lang').removeClass('is-open');
});
$(document).on('click', function(event){
    if( $(event.target).closest('.header__lang').length) 
        return;         
    $('.header__lang').removeClass('is-open');
    event.stopPropagation();
});
$(document).on('click', '.js-lk-edit', function(){
    $(this).closest('.lk-page').addClass('lk-page-edit')
    $(this).closest('.lk-page').find('.lk-page__input__text').prop('disabled', false);
    return false
});
if ($('.search-main').length) {
    $('.search-main__button').before($('.search-main__button').clone().attr('href', '#popup__search-main').attr('data-modal', 'text').addClass('mob'));
    $('.page').append('<div class="popup popup__search-main" id="popup__search-main"/>');
    $('#popup__search-main').append($('.search-main').clone());
    var $titleMobile = $('.search-main').data('title-mobile');
    $('#popup__search-main').prepend('<p class="popup__title">'+ $titleMobile +'</p>');
    $(".select2-search__field").prop("readonly", true);
}
//Маска телефона
$.masked = function () {
    var $el = $('.phone-masked');
    var maskList = $.masksSort($.masksLoad("json/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			showMaskOnHover: false,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask"
	};
    $el.inputmasks(maskOpts); 
}
if($('.phone-masked').length) {
    $.masked();
}
$(document).on('keyup focusout focusin', '.phone-masked', function() {
    if ($(this).inputmasks('isCompleted')){
        $(this).addClass('ok')
    }else {
        $(this).removeClass('ok')            
    }
});
$(document).on('click', '.header__bt-menu', function(){
    $(this).toggleClass('is-active');
    $('.header__control-user').toggleClass('is-active');
    $('html').toggleClass('open-menu');
    if(!$('html').hasClass('open-menu')) {
        $('.main').addClass('transition');
        setTimeout(function() {
            $('.main').removeClass('transition');
        }, 600);
    }
    return false
});
//$('.menu__button').before($('.menu__button').clone().attr('href', '#popup__search').attr('data-modal', 'text').addClass('mob'));

//popup
$(document).on('click', '[data-modal="text"]', function(e){

    e.preventDefault();
    var src = $(this).attr('href'), 
        bt = $(this);
    $.fancybox.open({
        src: src,
        type: 'inline',
        hash: false,
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#popup__search-main') {
                    $('.fancybox-slide').addClass('event-no')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }
    });    
});


$('.enter__sn').each(function(){
    $(this).closest('.popup__flexbox').find('.submit-box').before($(this).clone().addClass('mob'))  
});
$(document).on('click', '.js-recover', function(){
    $('.enter__sn').hide();
    $('.enter__recover').show();
    $(this).hide();
    return false 
});

$(document).on('click', '.js-popup-link', function(){
    var idLink = $(this).attr('href');
    $(this).closest('.popup').find(idLink).show();
    $(this).closest('.popup__cont').hide();
    return false 
});

$.fancyOptions = {
    type: 'image',
    buttons: [
        "smallBtn"
    ],
    thumbs: {
        autoStart: true,
        axis: 'x',
        hideOnClose: true,    
        parentEl: '.fancybox-container',  
    },
    afterShow: function() {
        $('.Valid').each(function(){
            $.Valid($(this));
        });
    },
    hash: false,
    loop: true,
    btnTpl: {
       smallBtn: '<button data-fancybox-close class="fancybox-close-small-img" title="{{CLOSE}}">' +
                    '<span>Закрыть</span>' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left">' +
                 '<svg style="width: 1.19rem; height: 0.81rem; transform: rotate(180deg);">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
              '</button>',

        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right">' +
                 '<svg style="width: 1.19rem; height: 0.81rem;">' +
                    '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-arrow"></use>' +
                 '</svg>' +
            '</button>'
    },
    lang: "ru",
    i18n: {
        ru: {
          CLOSE: "Закрыть",
          NEXT: "Вперёд",
          PREV: "Назад",
          ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
          PLAY_START: "Запустить слайдшоу",
          PLAY_STOP: "Остановить слайдшоу",
          FULL_SCREEN: "На весь экран",
          THUMBS: "Миниатюры",
          DOWNLOAD: "Скачать",
          SHARE: "Поделиться",
          ZOOM: "Увеличить"
        }
    },
    afterLoad: function(instance, current) {

        current.$content.closest('.fancybox-container').addClass('photo');
    }
}
$.fancy = function (el) {

    var $el = el;

    if (!$el.length) return;

    $el.attr('data-init', 'true');

    $el.fancybox($.fancyOptions);

};

//$.fancyAuto(
//    $('#popup__search-main')
//);
window.$(document).ready(function() {
    $.fancyAuto = function (el) {
        var $el = el;
        if (!$el.length) return;
    
        $.fancybox.open($el, {

            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small close" title="{{CLOSE}}">' +
                    '<svg>' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            }
        }).trigger('click');;
    }
});



$.fn.parallax = function (resistance, mouse) {
    $el = $(this);
    $elContainer = $el.parent();
    TweenLite.to($el, 0.1, {
        x: -((mouse.clientX - $elContainer.width() * 1.5) / resistance)*1.5,
        y: -((mouse.clientY - $elContainer.height() * 1.5) / resistance)*1.5,
        ease: Power1.easeInOut
    });
};
$(document).on('mousemove', '.js-parallax-container', function(e) {
    $(this).find(".js-parallax").parallax(-80, e);
});



$(document).ready(function(){
    $('.preloader__line').on('animationend', function(){
        $('.preloader').addClass('complide');
        setTimeout(function(){
            $('.preloader__img-f').addClass('active');
        }, 800);
        setTimeout(function(){
            $('.preloader').hide(0);
        }, 1500);
    });
});
$('.search-main__box').on('focus', 'input', function() { $(this).blur(); });
$(document).ready(function(){
    $('.preloader__line').on('animationend', function(){
        $('.preloader').addClass('complide');
        setTimeout(function(){
            $('.preloader').hide(0);
        }, 850);
    });
});

var swiper = new Swiper('.scroll-container', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    scrollbar: {
        el: '.swiper-scrollbar',
    },
    mousewheel: true,
    breakpoints: {
        0: {
            direction: 'horizontal',
        },
        767: {
            direction: 'vertical',
        },
    },
});
$(document).on('keyup paste', '.search__input', function(){
    var searchField = $(this).val();
    if(searchField.length >= 2){
        $.ajax({
            url: $(this).closest('form').attr('action') + '/search/'+searchField+'',
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                
                var regex = new RegExp(searchField, "i");
                var output = '<div>';
                var count = 1;
                $.each(data, function(key, val){
                    if ((val.title.search(regex) != -1)){
                        output += '<a href="'+ val.link +'" class="search__input-link"><dl class="search__input-link-dl">';
                        output += '<dt class="search__input-link-dt">' + val.title + '</dt>';
                        output += '<dd class="search__input-link-dd">' + val.length + '</dd>'
                        output += '</dl></a>';
                        count++;
                    }
                });
                output += '</div>';
                $('.search__inner .Valid').addClass('noval');
                var validator = $('.noval').validate();
                validator.destroy();
                $('.Valid:not(.noval)').each(function(){
                    $.Valid($(this));
                });
                $('.search__input-result').html(output).addClass('open');
                if($('.search__input-result').find('.search__input-link').length == false) {
                    $('.search__input-result').removeClass('open');
                }
                $(".search__input-link-dt").each(function(){
                    $(this).html($(this).text().replace(
                        new RegExp('(' + $('.search__input').val().replace(/(\[|\^|\$|\/|\\|\+|\*)/g, '\\$1') + ')', 'gi'),
                        '<span>$1</span>'
                    ));        
                });
            }
        })
    }else {
        $('.search__input-result').removeClass('open');
    }
});
$(document).ready(function(){
 
    if($('.search__input').length){
        if($('.search__input').val().length > 1){
            $('.search__inner .Valid').addClass('noval');
            var validator = $('.noval').validate();
            validator.destroy();
            $('.Valid:not(.noval)').each(function(){
                $.Valid($(this));
            });
        }
    }
});
$(document).on('blur', '.search__input', function(event){
    setTimeout(function(){
        $('.search__input-result').removeClass('open');
//        return false
    }, 500)
});
$.select = function(el) {
    var $el = el;

    if (!$el.length) return;
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
    
    var placeholder = $el.data('placeholder'); 
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: true,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        dropdownParent: dropdownParent,
        width: '100%',
        dropdownPosition: 'below'
    });
}
$.selectMulte = function(el) {
    var $el = el;

    if (!$el.length) return;
    
    var placeholder = $el.attr('placeholder'); 
    
    var dropdownParent = $(document.body);
    if ($el.parents('.select-container:not(.search-main__box-select)').length !== 0)
      dropdownParent = $el.parents('.select-container');
  
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: false,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        width: '100%',
        closeOnSelect: false,
        dropdownParent: dropdownParent,
        dropdownPosition: 'below',
        "language": {
           "noResults": function(){
               return "Ничего не найдено";
           }
       },
    });
    $el.each(function(){
        if($(this).attr('name') == 'c[]'){
            $(this).addClass('selectDirection').closest('.search-main__box-select').addClass('selectDirection__box');
            $(this).addClass('selectDirection').closest('.search__select-container').addClass('selectDirection__box');
            if($('html').hasClass('no-touchevents')){
                $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", false);
            }else {
                $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", true);
            }
        }else {
            $(this).closest('.select-container').find('input.select2-search__field').prop("readonly", true);
        }
    });
}


$('.multiple').on('select2:selecting', function(e) {
    var id = e.params.args.data.element.dataset.allselect;
    if(id == 'true') {
        $(this).find('option:not([data-allselect])').prop('selected', false);
    }else{
        $(this).find('option[data-allselect]').prop('selected', false);
    }
});
$('.multiple').on('select2:selecting', function(e) {
    $(this).closest('div').find('.select2-search__field').val(' ');
})
if($('.no-touchevents').length){
    $(document).on('mouseenter', '.tooltip-ok', function() {
        $('.tooltip-hide').addClass('is-active');
    });
    $(document).on('mouseleave', '.tooltip-ok', function() {
        $('.tooltip-hide').removeClass('is-active');
    });
};
if($('.touchevents').length){
    $(document).on('click', '.tooltip', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $('.multiple').select2("close");
        $('.tooltip-hide').toggleClass('is-active');
        return false
    });
};
$(document).on('click', function(event){
    if( $(event.target).closest('.selectDirection').length) 
        return;         
    $('.tooltip').removeClass('is-active');
    event.stopPropagation();
});
$.selectDirectionTooltip = function(el) {
    $('.selectDirection__box').addClass('tooltip-ok');
    $('.tooltip').remove();
    $('.tooltip-hide').remove();
    $('.selectDirection__box .select2-search').before('<li class="select2-selection__choice dropdown-toggle tooltip" href="javascript:;" data-toggle="dropdown">...</li>');
    if($('.search-main').length){
        $('.search-main').append('<div class="tooltip-hide"/>');
    }
    if($('.search__select-container').length){
        $('.search__select-container.selectDirection__box').append('<div class="tooltip-hide"/>');
    }
    var tooltipText = $('.selectDirection__box .select2-selection__choice:not(.tooltip)').text();
        tooltipText = tooltipText.replace('×', '');
        tooltipText = tooltipText.replace(/[×]/g, ", ");
    $('.tooltip-hide').text(tooltipText);
}
$('.multiple').on('select2:closing', function(e) {
    if($('.selectDirection').find('option:selected').length > 2 ){
        $.selectDirectionTooltip();
    }
 
    if($('.selectSize').length && $('.selectPrice').length) {
        
        if($(this).hasClass('selectSize')) {
            if($(this).find('option:selected').length > 0){
                if($(this).find('option[data-allselect]:selected').length !=1){
                    $('.selectPrice').find('option:not([data-allselect])').prop('disabled', true);
                    $(this).find('option:selected').each(function(){
                        var allLongMin = $(this).data('longfrom'),
                            allLongMax = $(this).data('longto');
                        if(allLongMin != null) {
                            $('.selectPrice option').each(function(){
                                if($(this).attr('data-longfrom') == allLongMin){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }
                        if(allLongMax != null) {
                            $('.selectPrice option').each(function(){
                                if($(this).attr('data-longto') == allLongMax){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }                    
                    });     
                }else {
                    $('.selectPrice').find('option').prop('disabled', false);
                }
            }else {
                $('.selectPrice').find('option').prop('disabled', false);
            }
        }
        if($(this).hasClass('selectPrice')) {
            if($(this).find('option:selected').length > 0){
                if($(this).find('option[data-allselect]:selected').length !=1){
                    $('.selectSize').find('option:not([data-allselect])').prop('disabled', true);
                    $(this).find('option:selected').each(function(){

                        var allLongMin = $(this).data('longfrom'),
                            allLongMax = $(this).data('longto');
                        if(allLongMin != null) {
                            $('.selectSize option').each(function(){
                                if($(this).attr('data-longfrom') == allLongMin){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }
                        if(allLongMax != null) {
                            $('.selectSize option').each(function(){
                                if($(this).attr('data-longto') == allLongMax){
                                    $(this).prop('disabled', false);
                                }
                            })
                        }                    
                    });     
                }else {
                    $('.selectSize').find('option').prop('disabled', false);
                }
            }else {
                $('.selectSize').find('option').prop('disabled', false);
            }
        }
    }    
})

$('.multiple').each(function(){
    $.selectMulte($(this));
});
$('.select').each(function(){
    $.select($(this));
});
if($('.direction__slider-big__shadow').length){
    $('.direction__slider-big__shadow').remove();
}
var swiper1 = new Swiper('.slider-direction', {
//    slidesPerView: 'auto',
    spaceBetween: 15,
    slideToClickedSlide: true,
    loop: true,
    centeredSlides: true,
    speed: 600,
    autoplay: {
        delay: 3000,
    },
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
            spaceBetween: 15,
            centeredSlides: true,
        },
        768: {
            slidesPerView: 'auto',
            spaceBetween: 0,
            centeredSlides: true,
        },
        890: {
            slidesPerView: 'auto',
            spaceBetween: 0,
            centeredSlides: true,
        },
        1024: {
            slidesPerView: 3,
            centeredSlides: true,
        },
    }
});

if($('.direction__slider-big__link').length){
//    $('.direction__slider-big__link').hide();
//    if($('.direction__slider-big__link').length == 1){
//        $('.direction__slider-big__link').first().show();
//    }else {
//        $('.direction__slider-big__link').eq(1).show();
//    }
    var loopRezDir = false;
    if($('.direction__slider-big__slide').length > 3){
        loopRezDir = true;
    }
    if($('.direction__slider-big__slide').length > 10 && $(window).width() <= 768){
        loopRezDir = false;
    }
}
var swiper = new Swiper('.direction__slider-big', {
    centeredSlides: true,
    slideToClickedSlide: true,
    initialSlide: 1,
    slidesOffsetBefore: 7,
    loop: true,
    pagination: {
         clickable: true, 
    },
    breakpoints: {
        0: {
            spaceBetween: 15,
            slidesPerView: 1.4,
            slidesOffsetBefore: 7,
        },
        480: {
            spaceBetween: 15,
            slidesPerView: 2,
        },
        1024: {
            spaceBetween: 103,
            slidesPerView: 2,
        },
        1400: {
            spaceBetween: 103,
            slidesPerView: 3,
        },
        1950: {
            spaceBetween: 103,
            slidesPerView: 3,
        },
        2025: {
            spaceBetween: 103,
            slidesPerView: 4,
        },
    }
});
const swiperSlides = document.getElementsByClassName('direction__slider-big__slide');

var swiperDirectionNav = new Swiper('.direction__slider-big-nav', {
    spaceBetween: 0,
    speed: 600,
    slidesPerView: 'auto',
    loop: true,
    pagination: {
        clickable: true, 
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      
    }
});
if($('.direction__slider-big').length){
    $(document).on('click', '.direction__slider-big-nav__slide', function (e) {
        e.preventDefault();
        $(this).addClass('direction__slider-big-nav__slide_active').siblings('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active');
        var linkSlide = $(this).attr('data-link'),
            nam = $('.direction__slider-big__slide[data-direction=' + linkSlide+']').index();
        swiper.slideTo(nam, 1000, false);
        $('.direction__slider-big-nav__slide[data-link="'+linkSlide+'"]').addClass('direction__slider-big-nav__slide_active');
    });        
    swiper.on('slideChange', function (e) {
        if($('.direction__slider-big-nav').length){
            var nameGroup = $('.direction__slider-big__slide').eq(swiper.activeIndex).data('direction'),
                selectGroup = $('.direction__slider-big-nav__slide[data-link=' + nameGroup+']');
            swiperDirectionNav.slideTo(selectGroup.index(), 1000, false);
            $('.direction__slider-big-nav__slide').removeClass('direction__slider-big-nav__slide_active');
            $('.direction__slider-big-nav__slide[data-link='+ nameGroup +']').addClass('direction__slider-big-nav__slide_active');
            
        }
    });    
}
$.faqSlider = function() {
    let faqSliderSelector = $('.faq-slider'); 

    faqSliderSelector.each(function(){
        var mySwiper = new Swiper(this, {
            spaceBetween: 0,
            mousewheel: false,
            speed: 600,
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                    allowSlideNext: true,
                    allowSlidePrev: true,
                    spaceBetween: 0
                    
                },
                615: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 30
                },
                900: {
                    slidesPerView: 3,
                    allowSlideNext: false,
                    allowSlidePrev: false,
                    spaceBetween: 0
                }
            }
        })
    })
}
var galleryThumbs = new Swiper('.destinations__slider-nav', {
    spaceBetween: 0,
    slideToClickedSlide: true,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 'auto',
        },
        480: {
            slidesPerView: 2,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    },
});
var galleryTop = new Swiper('.destinations__slider', {
    spaceBetween: 70,
    thumbs: {
        swiper: galleryThumbs
    }
});
if($('.detailed__slider').length) {
    var loopRez = false;
    if($('.detailed__slider__slide').length > 4){
        loopRez = true;
    }
    var swiperDirectionLit = new Swiper('.detailed__slider', {
        spaceBetween: 0,
        slideToClickedSlide: true,
        loopedSlides: 0,
        speed: 600,
        spaceBetween: 35,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            0: {
               slidesPerView: 'auto',
               centeredSlides: true,
                loop: false,  
            },
            480: {
                slidesPerView: 2,
            },
            768: {
                slidesPerView: 3,
            },
            1024: {
                loop: loopRez,  
                slidesPerView: 4,
            },
        }
    });
}
var detailedSlider = new Swiper('.detailed-description__slider', {
    spaceBetween: 0,
    speed: 600,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});
var catalogSliderMobile = new Swiper('.catalog__slidr-mobile', {
    
    slidesPerView: 'auto',
    speed: 600,
    breakpoints: {
        0: {
            slidesPerView: 'auto',
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        480: {
            slidesPerView: 2,
            spaceBetween: 20,
            centeredSlides: true,
            initialSlide: 1,
            slideActiveClass: "catalog__li_active"
        },
        768: {
            slidesPerView: 3,
            spaceBetween: 20,
            centeredSlides: false,
            slideActiveClass: "catalog__li_active"
        },
        1024: {
            slidesPerView: 3,
            spaceBetween: 77,
            centeredSlides: false,
        },
    }
});
$.fancy(
    $('[data-fancybox="image"]')
);

$('.detailed__slider__link-tabs').each(function(){
    var numberEl = $(this).closest('.detailed__slider__slide').index();
    $(this).attr('data-link', numberEl);
});
$(document).on('click', '.detailed__slider__link-tabs', function(){
    $('html, body').animate({
        scrollTop: $(".detailed-description__tabs").offset().top - 200
    }, 1000);
    var link = $(this).data('link');
    $('.detailed-description__tabs li').eq(link).trigger('click');
    return false
});
$(document).on('click', '.tabs-link-js:not(.current)', function() {
    $(this).addClass('current').siblings().removeClass('current')
        .parents('.tabs-js').find('.js-tabs__box').eq($(this).index()).fadeIn().siblings('.js-tabs__box').hide();
    return false
});
var originalVal = $.fn.val;
$.fn.val = function(){
    var result =originalVal.apply(this,arguments);
    if(arguments.length>0)
        $(this).change(); // OR with custom event $(this).trigger('value-changed');
    return result;
};
$(document).on('propertychange change click keyup input paste', '.textarea-counter', function(e){
    var max = $(this).attr('maxlength'),
    text_len = $(this).val().length,
    count = max - text_len;
    $(this).closest('.counter__container').find('.counter').html(count);
    originalVal();
});
//validator
$.Valid = function (el) {

    var $el = el;

    if (!$el.length) return;
    var validator = $el.validate({
        rules: {
            name: {
                required: true
            },
            first_name: {
                required: true
            },
            surname: {
                required: true
            },
            password: {
                required: true
            },
            password1: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            password1_confirmation: {
                required: true,
                minlength: 6,
                equalTo: '[name="password1"]'
            },
            tel: {
                required: true,
                checkMask: true
            },
            phone: {
                required: true,
                checkMask: true
            },
            check: {
                required: true
            },
            textarea: {
                required: true
            },
            message: {
                required: true
            },
            email: {
                required: true,
                mailVal: true
            },
            date: {
                required: true
            },
            from: {
                required: true
            },
            'c[]': {
                required: true
            },
            submitHandler: function (form) {
                if($(form).hasClass('validLk')){
                    $(form).closest('.lk-page').removeClass('lk-page-edit')
                    $(form).find('.lk-page__input__text').prop('disabled', true);
                    setTimeout(function() {
                        form.submit();
                    }, 2500);
                }else if($(form).hasClass('contacts-form')){
                    var msg   = $(form).serialize(),
                        url = $(form).attr('action');
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: msg,
                        success: function(data) {
                            if(data.result == true || data.result == "ok"){   
                                $('.contacts__box-text').hide();
                                $('.send__ok').show();                                
                            }else {
                                $(form).append('<p class="error-send">' + data.error + '</p>')
                                $('.contacts__box-text').hide();
                                $('.send__error').show();
                            }
                            
                        },
                        error:  function(xhr, str){

                        }
                    });
                }else {
                    form.submit();
                }
            }
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').addClass('error');
            $(element).addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.Valid').removeClass('error');
            $(element).removeClass('error');
        }
    })
    $.validator.addMethod("pwcheck", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) 
           && /[a-z]/.test(value) 
           && /\d/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("mailVal", function(value) {
       return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("checkMask", function(value) {
         return $('.phone-masked.ok').length;
    });
};
$('.Valid:not(.noval)').each(function(){
    $.Valid($(this));
});
